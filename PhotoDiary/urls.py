from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'PhotoDiary.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$','cliders.views.index'),
    
    url(r'^LoginError/$','cliders.views.LoginError'),
    url(r'^GetUserInfo/$', 'cliders.views.GetUserInfo'),
    url(r'^Home/(?P<yymm>.+)/$','cliders.views.Home'),
    url(r'^Home/$','cliders.views.Home'),
    # cliders Setting urls:
    url(r'^AddDiary/','cliders.views.AddDiary'),
    url(r'^EditDiary/(?P<pageid>.+)/$','cliders.views.AddDiary'),
    url(r'^DiarySave/','cliders.views.DiarySave'),
    url(r'^UserDiary/','cliders.views.UserDiary'),
    url(r'^Diary/(?P<pageid>.+)/$','cliders.views.PresentDiary'),
    url(r'^FBShare/','cliders.views.FacebookShare'),
    url(r'^DeleteDiary/','cliders.views.DeleteDiary'),
    url(r'^AddSlide/','cliders.views.AddSlide'),
    url(r'^DiaryImgSave/','cliders.views.DiaryImgSave'),
    url(r'^SaveSlideOrder/','cliders.views.SaveSlideOrder'),
    url(r'^Logout/$', 'cliders.views.Logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'',include('social.apps.django_app.urls',namespace='social')),#django social auth
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
