﻿# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 10.68.68.139 (MySQL 10.0.5-MariaDB-1~wheezy-log)
# Database: clidersv2
# Generation Time: 2014-04-09 11:02:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add log entry',1,'add_logentry'),
	(2,'Can change log entry',1,'change_logentry'),
	(3,'Can delete log entry',1,'delete_logentry'),
	(4,'Can add permission',2,'add_permission'),
	(5,'Can change permission',2,'change_permission'),
	(6,'Can delete permission',2,'delete_permission'),
	(7,'Can add group',3,'add_group'),
	(8,'Can change group',3,'change_group'),
	(9,'Can delete group',3,'delete_group'),
	(10,'Can add user',4,'add_user'),
	(11,'Can change user',4,'change_user'),
	(12,'Can delete user',4,'delete_user'),
	(13,'Can add content type',5,'add_contenttype'),
	(14,'Can change content type',5,'change_contenttype'),
	(15,'Can delete content type',5,'delete_contenttype'),
	(16,'Can add session',6,'add_session'),
	(17,'Can change session',6,'change_session'),
	(18,'Can delete session',6,'delete_session'),
	(19,'Can add user social auth',7,'add_usersocialauth'),
	(20,'Can change user social auth',7,'change_usersocialauth'),
	(21,'Can delete user social auth',7,'delete_usersocialauth'),
	(22,'Can add nonce',8,'add_nonce'),
	(23,'Can change nonce',8,'change_nonce'),
	(24,'Can delete nonce',8,'delete_nonce'),
	(25,'Can add association',9,'add_association'),
	(26,'Can change association',9,'change_association'),
	(27,'Can delete association',9,'delete_association'),
	(28,'Can add code',10,'add_code'),
	(29,'Can change code',10,'change_code'),
	(30,'Can delete code',10,'delete_code'),
	(31,'Can add migration history',11,'add_migrationhistory'),
	(32,'Can change migration history',11,'change_migrationhistory'),
	(33,'Can delete migration history',11,'delete_migrationhistory'),
	(34,'Can add member',12,'add_member'),
	(35,'Can change member',12,'change_member'),
	(36,'Can delete member',12,'delete_member'),
	(37,'Can add diary',13,'add_diary'),
	(38,'Can change diary',13,'change_diary'),
	(39,'Can delete diary',13,'delete_diary'),
	(40,'Can add slider',14,'add_slider'),
	(41,'Can change slider',14,'change_slider'),
	(42,'Can delete slider',14,'delete_slider'),
	(43,'Can add slider_photo',15,'add_slider_photo'),
	(44,'Can change slider_photo',15,'change_slider_photo'),
	(45,'Can delete slider_photo',15,'delete_slider_photo'),
	(46,'Can add photo',16,'add_photo'),
	(47,'Can change photo',16,'change_photo'),
	(48,'Can delete photo',16,'delete_photo'),
	(49,'Can add frame',17,'add_frame'),
	(50,'Can change frame',17,'change_frame'),
	(51,'Can delete frame',17,'delete_frame'),
	(52,'Can add diary theme',18,'add_diarytheme'),
	(53,'Can change diary theme',18,'change_diarytheme'),
	(54,'Can delete diary theme',18,'delete_diarytheme'),
	(55,'Can add news',19,'add_news'),
	(56,'Can change news',19,'change_news'),
	(57,'Can delete news',19,'delete_news'),
	(58,'Can add news_photo',20,'add_news_photo'),
	(59,'Can change news_photo',20,'change_news_photo'),
	(60,'Can delete news_photo',20,'delete_news_photo');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`)
VALUES
	(1,'pbkdf2_sha256$12000$NMdpDdpIJaiO$ZB6ovQuZ/eyYmYR27eOw+CAF4cZWip5wwsWjGSH1JbI=','2014-03-27 06:45:38',1,'donz','','','donz@donz.tw',1,1,'2014-03-14 08:11:15'),
	(5,'!QpDnhxOZ6lNCslgXdTST8kXAU6elBZOgxz8zV8z7','2014-03-25 10:04:07',0,'chen.eric.9461','Chen','Eric','justlove0738@yahoo.com.tw',0,1,'2014-03-14 08:43:43'),
	(6,'!pqLAKybQSz98mjuFYpgy6RTTPOO6tF7lvXs0PhKk','2014-04-07 01:26:00',0,'hunghsi.ku','泓熙','辜','justlove0714@hotmail.com',0,1,'2014-03-14 08:44:59');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cliders_diary
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_diary`;

CREATE TABLE `cliders_diary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `diary_id` varchar(50) NOT NULL,
  `diary_content` varchar(5000) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_diary_b3c09425` (`member_id`),
  CONSTRAINT `member_id_refs_id_0b604fc9` FOREIGN KEY (`member_id`) REFERENCES `cliders_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cliders_diary` WRITE;
/*!40000 ALTER TABLE `cliders_diary` DISABLE KEYS */;

INSERT INTO `cliders_diary` (`id`, `member_id`, `diary_id`, `diary_content`, `date`, `title`)
VALUES
	(694,5,'e505c8c2158a42768dc5fdcee98ea219','<img src=\"/media/image/resize_photo/e505c8c2158a42768dc5fdcee98ea219/1fa11a83bef54cbdb50573905e29c514.jpeg\">','2014-03-27','(none)'),
	(695,5,'db2722fbfd494f0f81fb1468d8b93847','<img src=\"/media/image/resize_photo/db2722fbfd494f0f81fb1468d8b93847/c9831493cbcd4803bc1fdd73d157f044.jpeg\"><img src=\"/media/image/resize_photo/db2722fbfd494f0f81fb1468d8b93847/6cc749bfcd434b5bb0c4e3eb853142d0.jpeg\">','2014-03-27','(none)'),
	(696,5,'541e55011bb342359a74cd5112434050','<img src=\"/media/image/resize_photo/541e55011bb342359a74cd5112434050/547f7bdb9dd54ea6ba5b59ce514b340d.jpeg\"><img src=\"/media/image/resize_photo/541e55011bb342359a74cd5112434050/e9d7ba068fe74c70b3a066f2c3bdc654.jpeg\"><img src=\"/media/image/resize_photo/541e55011bb342359a74cd5112434050/1c46911e993644f79b3712dd5f48e54a.jpeg\">','2014-03-28','(none)'),
	(697,5,'d150d0b114cb4f3aa41bf90357371ca5','<img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/d0c41753914140309661b1c0fc936fe9.jpeg\"><img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/1a40b96fffb84180a3fb22b5057665fd.jpeg\"><img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/68a2100509294d50a8ed7bb9334c2e70.jpeg\"><img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/88259d5e13b34df6be222ab10f4f5f2a.jpeg\"><img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/9da97b62beeb4a7f999b0b67d5f270e6.jpeg\"><img src=\"/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/1ece64af157c40ccb404d44f71732778.jpeg\">','2014-04-01','(none)'),
	(698,5,'73c041dd80a94bbd8872a22df398dc46','','2014-04-01',NULL),
	(699,5,'6c79ab7a5ec84b0ba1b3c624447eb123','','2014-04-01',NULL),
	(700,5,'eda9928964eb42df8556b12ec1b0d74d','','2014-04-01',NULL);

/*!40000 ALTER TABLE `cliders_diary` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliders_diarytheme
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_diarytheme`;

CREATE TABLE `cliders_diarytheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diary_id` int(11) NOT NULL,
  `theme` int(11) NOT NULL,
  `background_image` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `button` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_diarytheme_67548958` (`diary_id`),
  CONSTRAINT `diary_id_refs_id_b97e2696` FOREIGN KEY (`diary_id`) REFERENCES `cliders_diary` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cliders_frame
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_frame`;

CREATE TABLE `cliders_frame` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diary_id` int(11) NOT NULL,
  `color` varchar(10) NOT NULL,
  `background_image` varchar(100) NOT NULL,
  `theme` int(11) DEFAULT NULL,
  `background_color` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_frame_67548958` (`diary_id`),
  CONSTRAINT `diary_id_refs_id_7bc87a31` FOREIGN KEY (`diary_id`) REFERENCES `cliders_diary` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cliders_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_member`;

CREATE TABLE `cliders_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `birthday` date,
  `head_image` varchar(250),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_eca33931` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cliders_member` WRITE;
/*!40000 ALTER TABLE `cliders_member` DISABLE KEYS */;

INSERT INTO `cliders_member` (`id`, `user_id`, `name`, `email`, `sex`, `birthday`, `head_image`)
VALUES
	(4,5,'Chen Eric','justlove0738@yahoo.com.tw','male','1987-03-03','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-frc1/t1.0-1/c178.0.604.604/s200x200/252231_1002029915278_1941483569_n.jpg'),
	(5,6,'辜泓熙','justlove0714@hotmail.com','male','1987-07-16','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn2/t1.0-1/c62.65.466.466/s200x200/179841_1600601261274_2424882_n.jpg');

/*!40000 ALTER TABLE `cliders_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliders_news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_news`;

CREATE TABLE `cliders_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `content` varchar(2500) DEFAULT NULL,
  `title` varchar(500) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cliders_news_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_news_photo`;

CREATE TABLE `cliders_news_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_news_photo_b4473b54` (`news_id`),
  CONSTRAINT `news_id_refs_id_398232f4` FOREIGN KEY (`news_id`) REFERENCES `cliders_news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cliders_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_photo`;

CREATE TABLE `cliders_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diary_id` int(11) NOT NULL,
  `origin_image` varchar(100) NOT NULL,
  `resize_image` varchar(100) NOT NULL,
  `positionLeft` double NOT NULL,
  `positionRight` double NOT NULL,
  `zoom` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_photo_67548958` (`diary_id`),
  CONSTRAINT `diary_id_refs_id_d75dd826` FOREIGN KEY (`diary_id`) REFERENCES `cliders_diary` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cliders_photo` WRITE;
/*!40000 ALTER TABLE `cliders_photo` DISABLE KEYS */;

INSERT INTO `cliders_photo` (`id`, `diary_id`, `origin_image`, `resize_image`, `positionLeft`, `positionRight`, `zoom`)
VALUES
	(216,694,'image/ori_photo/e505c8c2158a42768dc5fdcee98ea219/38aca17c2d694213bb8d72a20dc73b89.jpeg','image/resize_photo/e505c8c2158a42768dc5fdcee98ea219/1fa11a83bef54cbdb50573905e29c514.jpeg',0,0,1),
	(217,695,'image/ori_photo/db2722fbfd494f0f81fb1468d8b93847/0eaf14937f3e41df9305636362f5e5c7.jpeg','image/resize_photo/db2722fbfd494f0f81fb1468d8b93847/6cc749bfcd434b5bb0c4e3eb853142d0.jpeg',0,0,1),
	(218,695,'image/ori_photo/db2722fbfd494f0f81fb1468d8b93847/968b05791b214ccb9be8753882d49806.jpeg','image/resize_photo/db2722fbfd494f0f81fb1468d8b93847/c9831493cbcd4803bc1fdd73d157f044.jpeg',0,0,1),
	(219,696,'image/ori_photo/541e55011bb342359a74cd5112434050/924f7cbb2edf4006a9dc0c4e83e662e8.jpeg','image/resize_photo/541e55011bb342359a74cd5112434050/547f7bdb9dd54ea6ba5b59ce514b340d.jpeg',0,0,1),
	(220,696,'image/ori_photo/541e55011bb342359a74cd5112434050/8b0ebb44f82948cdb0edb364a3c4b1d4.jpeg','image/resize_photo/541e55011bb342359a74cd5112434050/e9d7ba068fe74c70b3a066f2c3bdc654.jpeg',0,0,1),
	(221,696,'image/ori_photo/541e55011bb342359a74cd5112434050/5aa6af33969d4290894271b4977a049a.jpeg','image/resize_photo/541e55011bb342359a74cd5112434050/1c46911e993644f79b3712dd5f48e54a.jpeg',0,0,1),
	(222,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/ea5d1b15ceac415a9ecadaadf3826f86.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/d0c41753914140309661b1c0fc936fe9.jpeg',0,0,1),
	(223,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/f6143b7e53e340539a7e2a5aaa4500f5.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/1a40b96fffb84180a3fb22b5057665fd.jpeg',0,0,1),
	(224,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/f6ccba519cbd42c3bf5d84d3748ac6f1.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/68a2100509294d50a8ed7bb9334c2e70.jpeg',0,0,1),
	(225,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/bef2e57fd12e4640baf36c40dd04c5f5.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/88259d5e13b34df6be222ab10f4f5f2a.jpeg',0,0,1),
	(226,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/3daed1c0134c475382751df793a726cd.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/9da97b62beeb4a7f999b0b67d5f270e6.jpeg',0,0,1),
	(227,697,'image/ori_photo/d150d0b114cb4f3aa41bf90357371ca5/bf92321dd5f541a286f3d3229d844836.jpeg','image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/1ece64af157c40ccb404d44f71732778.jpeg',0,0,1);

/*!40000 ALTER TABLE `cliders_photo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliders_slider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_slider`;

CREATE TABLE `cliders_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(75) NOT NULL,
  `slider_id` varchar(50) NOT NULL,
  `describe` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_slider_b3c09425` (`member_id`),
  CONSTRAINT `member_id_refs_id_4a13b6ea` FOREIGN KEY (`member_id`) REFERENCES `cliders_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cliders_slider` WRITE;
/*!40000 ALTER TABLE `cliders_slider` DISABLE KEYS */;

INSERT INTO `cliders_slider` (`id`, `member_id`, `date`, `title`, `slider_id`, `describe`)
VALUES
	(1,5,'2014-04-01','','5ef8f0ced3e54606afff87e3eafb9560',''),
	(2,5,'2014-04-07','','26e22979764c43ef91ec1a3c83c372da',''),
	(3,5,'2014-04-08','','7268b11f13434461b6b4b38e7e7f0a4f','');

/*!40000 ALTER TABLE `cliders_slider` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliders_slider_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliders_slider_photo`;

CREATE TABLE `cliders_slider_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `image_url` varchar(600),
  `describe` varchar(75) NOT NULL,
  `order` int(11) NOT NULL,
  `positionLeft` double NOT NULL,
  `positionRight` double NOT NULL,
  `zoom` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliders_slider_photo_24685af0` (`slider_id`),
  CONSTRAINT `slider_id_refs_id_be21b129` FOREIGN KEY (`slider_id`) REFERENCES `cliders_slider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cliders_slider_photo` WRITE;
/*!40000 ALTER TABLE `cliders_slider_photo` DISABLE KEYS */;

INSERT INTO `cliders_slider_photo` (`id`, `slider_id`, `image_url`, `describe`, `order`, `positionLeft`, `positionRight`, `zoom`)
VALUES
	(1,1,'http://clide.rs:8001/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/1a40b96fffb84180a3fb22b5057665fd.jpeg','',0,0,0,1),
	(2,1,'http://clide.rs:8001/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/68a2100509294d50a8ed7bb9334c2e70.jpeg','',1,0,0,1),
	(3,1,'http://clide.rs:8001/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/88259d5e13b34df6be222ab10f4f5f2a.jpeg','',2,0,0,1),
	(4,2,'http://clide.rs:8001/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/d0c41753914140309661b1c0fc936fe9.jpeg','',0,0,0,1),
	(5,3,'http://clide.rs:8001/media/image/resize_photo/d150d0b114cb4f3aa41bf90357371ca5/d0c41753914140309661b1c0fc936fe9.jpeg','',0,0,0,1);

/*!40000 ALTER TABLE `cliders_slider_photo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_admin_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_admin_log`;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;

INSERT INTO `django_admin_log` (`id`, `action_time`, `user_id`, `content_type_id`, `object_id`, `object_repr`, `action_flag`, `change_message`)
VALUES
	(1,'2014-03-20 05:55:33',1,16,'150','Photo object',3,''),
	(2,'2014-03-20 05:55:33',1,16,'149','Photo object',3,''),
	(3,'2014-03-20 05:55:33',1,16,'148','Photo object',3,''),
	(4,'2014-03-20 05:55:33',1,16,'147','Photo object',3,''),
	(5,'2014-03-20 05:55:33',1,16,'146','Photo object',3,''),
	(6,'2014-03-20 05:55:33',1,16,'145','Photo object',3,''),
	(7,'2014-03-20 05:55:33',1,16,'144','Photo object',3,''),
	(8,'2014-03-20 05:55:33',1,16,'143','Photo object',3,''),
	(9,'2014-03-20 05:55:33',1,16,'142','Photo object',3,''),
	(10,'2014-03-20 05:55:33',1,16,'141','Photo object',3,''),
	(11,'2014-03-20 05:55:33',1,16,'140','Photo object',3,''),
	(12,'2014-03-20 05:55:33',1,16,'139','Photo object',3,''),
	(13,'2014-03-20 05:55:33',1,16,'138','Photo object',3,''),
	(14,'2014-03-20 05:55:33',1,16,'137','Photo object',3,''),
	(15,'2014-03-20 05:55:33',1,16,'136','Photo object',3,''),
	(16,'2014-03-20 05:55:33',1,16,'135','Photo object',3,''),
	(17,'2014-03-20 05:55:33',1,16,'134','Photo object',3,''),
	(18,'2014-03-20 05:55:33',1,16,'133','Photo object',3,''),
	(19,'2014-03-20 05:55:33',1,16,'132','Photo object',3,''),
	(20,'2014-03-20 05:55:33',1,16,'131','Photo object',3,''),
	(21,'2014-03-20 05:55:33',1,16,'130','Photo object',3,''),
	(22,'2014-03-20 05:55:33',1,16,'129','Photo object',3,''),
	(23,'2014-03-20 05:55:33',1,16,'128','Photo object',3,''),
	(24,'2014-03-20 05:55:33',1,16,'127','Photo object',3,''),
	(25,'2014-03-20 05:55:33',1,16,'126','Photo object',3,''),
	(26,'2014-03-20 05:55:33',1,16,'125','Photo object',3,''),
	(27,'2014-03-20 05:55:33',1,16,'124','Photo object',3,''),
	(28,'2014-03-20 05:55:33',1,16,'123','Photo object',3,''),
	(29,'2014-03-20 05:55:33',1,16,'122','Photo object',3,''),
	(30,'2014-03-20 05:55:33',1,16,'121','Photo object',3,''),
	(31,'2014-03-20 05:55:33',1,16,'120','Photo object',3,''),
	(32,'2014-03-20 05:55:33',1,16,'119','Photo object',3,''),
	(33,'2014-03-20 05:55:33',1,16,'118','Photo object',3,''),
	(34,'2014-03-20 05:55:33',1,16,'117','Photo object',3,''),
	(35,'2014-03-20 05:55:33',1,16,'116','Photo object',3,''),
	(36,'2014-03-20 05:55:33',1,16,'115','Photo object',3,''),
	(37,'2014-03-20 05:55:33',1,16,'114','Photo object',3,''),
	(38,'2014-03-20 05:55:33',1,16,'113','Photo object',3,''),
	(39,'2014-03-20 05:55:33',1,16,'112','Photo object',3,''),
	(40,'2014-03-20 05:55:33',1,16,'111','Photo object',3,''),
	(41,'2014-03-20 05:55:33',1,16,'110','Photo object',3,''),
	(42,'2014-03-20 05:55:33',1,16,'109','Photo object',3,''),
	(43,'2014-03-20 05:55:33',1,16,'108','Photo object',3,''),
	(44,'2014-03-20 05:55:33',1,16,'107','Photo object',3,''),
	(45,'2014-03-20 05:55:33',1,16,'106','Photo object',3,''),
	(46,'2014-03-20 05:55:33',1,16,'105','Photo object',3,''),
	(47,'2014-03-20 05:55:33',1,16,'104','Photo object',3,''),
	(48,'2014-03-20 05:55:33',1,16,'103','Photo object',3,''),
	(49,'2014-03-20 05:55:33',1,16,'102','Photo object',3,''),
	(50,'2014-03-20 05:55:33',1,16,'101','Photo object',3,''),
	(51,'2014-03-20 05:55:33',1,16,'100','Photo object',3,''),
	(52,'2014-03-20 05:55:33',1,16,'99','Photo object',3,''),
	(53,'2014-03-20 05:55:33',1,16,'98','Photo object',3,''),
	(54,'2014-03-20 05:55:33',1,16,'97','Photo object',3,''),
	(55,'2014-03-20 05:55:33',1,16,'96','Photo object',3,''),
	(56,'2014-03-20 05:55:33',1,16,'95','Photo object',3,''),
	(57,'2014-03-20 05:55:33',1,16,'94','Photo object',3,''),
	(58,'2014-03-20 05:55:33',1,16,'93','Photo object',3,''),
	(59,'2014-03-20 05:55:33',1,16,'92','Photo object',3,''),
	(60,'2014-03-20 05:55:33',1,16,'91','Photo object',3,''),
	(61,'2014-03-20 05:55:33',1,16,'90','Photo object',3,''),
	(62,'2014-03-20 05:55:33',1,16,'89','Photo object',3,''),
	(63,'2014-03-20 05:55:33',1,16,'88','Photo object',3,''),
	(64,'2014-03-20 05:55:33',1,16,'87','Photo object',3,''),
	(65,'2014-03-20 05:55:33',1,16,'86','Photo object',3,''),
	(66,'2014-03-20 05:55:33',1,16,'85','Photo object',3,''),
	(67,'2014-03-20 05:55:33',1,16,'84','Photo object',3,''),
	(68,'2014-03-20 05:55:33',1,16,'83','Photo object',3,''),
	(69,'2014-03-20 05:55:33',1,16,'82','Photo object',3,''),
	(70,'2014-03-20 05:55:33',1,16,'81','Photo object',3,''),
	(71,'2014-03-20 05:55:33',1,16,'80','Photo object',3,''),
	(72,'2014-03-20 05:55:33',1,16,'79','Photo object',3,''),
	(73,'2014-03-20 05:55:33',1,16,'78','Photo object',3,''),
	(74,'2014-03-20 05:55:33',1,16,'77','Photo object',3,''),
	(75,'2014-03-20 05:55:33',1,16,'76','Photo object',3,''),
	(76,'2014-03-20 05:55:33',1,16,'75','Photo object',3,''),
	(77,'2014-03-20 05:55:33',1,16,'74','Photo object',3,''),
	(78,'2014-03-20 05:55:33',1,16,'73','Photo object',3,''),
	(79,'2014-03-20 05:55:33',1,16,'72','Photo object',3,''),
	(80,'2014-03-20 05:55:33',1,16,'71','Photo object',3,''),
	(81,'2014-03-20 05:55:33',1,16,'70','Photo object',3,''),
	(82,'2014-03-20 05:55:33',1,16,'69','Photo object',3,''),
	(83,'2014-03-20 05:55:33',1,16,'68','Photo object',3,''),
	(84,'2014-03-20 05:55:33',1,16,'67','Photo object',3,''),
	(85,'2014-03-20 05:55:33',1,16,'66','Photo object',3,''),
	(86,'2014-03-20 05:55:33',1,16,'65','Photo object',3,''),
	(87,'2014-03-20 05:55:33',1,16,'64','Photo object',3,''),
	(88,'2014-03-20 05:55:33',1,16,'63','Photo object',3,''),
	(89,'2014-03-20 05:55:33',1,16,'62','Photo object',3,''),
	(90,'2014-03-20 05:55:33',1,16,'61','Photo object',3,''),
	(91,'2014-03-20 05:55:33',1,16,'60','Photo object',3,''),
	(92,'2014-03-20 05:55:33',1,16,'59','Photo object',3,''),
	(93,'2014-03-20 05:55:33',1,16,'58','Photo object',3,''),
	(94,'2014-03-20 05:55:33',1,16,'57','Photo object',3,''),
	(95,'2014-03-20 05:55:33',1,16,'56','Photo object',3,''),
	(96,'2014-03-20 05:55:33',1,16,'55','Photo object',3,''),
	(97,'2014-03-20 05:55:33',1,16,'54','Photo object',3,''),
	(98,'2014-03-20 05:55:33',1,16,'53','Photo object',3,''),
	(99,'2014-03-20 05:55:33',1,16,'52','Photo object',3,''),
	(100,'2014-03-20 05:55:33',1,16,'51','Photo object',3,''),
	(101,'2014-03-20 05:55:40',1,16,'50','Photo object',3,''),
	(102,'2014-03-20 05:55:40',1,16,'49','Photo object',3,''),
	(103,'2014-03-20 05:55:40',1,16,'48','Photo object',3,''),
	(104,'2014-03-20 05:55:40',1,16,'47','Photo object',3,''),
	(105,'2014-03-20 05:55:40',1,16,'46','Photo object',3,''),
	(106,'2014-03-20 05:55:40',1,16,'45','Photo object',3,''),
	(107,'2014-03-20 05:55:40',1,16,'44','Photo object',3,''),
	(108,'2014-03-20 05:55:40',1,16,'43','Photo object',3,''),
	(109,'2014-03-20 05:55:40',1,16,'42','Photo object',3,''),
	(110,'2014-03-20 05:55:40',1,16,'41','Photo object',3,''),
	(111,'2014-03-20 05:55:40',1,16,'40','Photo object',3,''),
	(112,'2014-03-20 05:55:40',1,16,'39','Photo object',3,''),
	(113,'2014-03-20 05:55:40',1,16,'38','Photo object',3,''),
	(114,'2014-03-20 05:55:40',1,16,'37','Photo object',3,''),
	(115,'2014-03-20 05:55:40',1,16,'36','Photo object',3,''),
	(116,'2014-03-20 05:55:40',1,16,'35','Photo object',3,''),
	(117,'2014-03-20 05:55:40',1,16,'34','Photo object',3,''),
	(118,'2014-03-20 05:55:40',1,16,'33','Photo object',3,''),
	(119,'2014-03-20 05:55:40',1,16,'32','Photo object',3,''),
	(120,'2014-03-20 05:55:40',1,16,'31','Photo object',3,''),
	(121,'2014-03-20 05:55:40',1,16,'30','Photo object',3,''),
	(122,'2014-03-20 05:55:40',1,16,'29','Photo object',3,''),
	(123,'2014-03-20 05:55:40',1,16,'28','Photo object',3,''),
	(124,'2014-03-20 05:55:40',1,16,'27','Photo object',3,''),
	(125,'2014-03-20 05:55:40',1,16,'26','Photo object',3,''),
	(126,'2014-03-20 05:55:40',1,16,'25','Photo object',3,''),
	(127,'2014-03-20 05:55:40',1,16,'24','Photo object',3,''),
	(128,'2014-03-20 05:55:40',1,16,'23','Photo object',3,''),
	(129,'2014-03-20 05:55:40',1,16,'22','Photo object',3,''),
	(130,'2014-03-20 05:55:40',1,16,'21','Photo object',3,''),
	(131,'2014-03-20 05:55:40',1,16,'20','Photo object',3,''),
	(132,'2014-03-20 05:55:40',1,16,'19','Photo object',3,''),
	(133,'2014-03-20 05:55:40',1,16,'18','Photo object',3,''),
	(134,'2014-03-20 05:55:40',1,16,'17','Photo object',3,''),
	(135,'2014-03-20 05:55:40',1,16,'16','Photo object',3,''),
	(136,'2014-03-20 05:55:40',1,16,'15','Photo object',3,''),
	(137,'2014-03-20 05:55:40',1,16,'14','Photo object',3,''),
	(138,'2014-03-20 05:55:40',1,16,'13','Photo object',3,''),
	(139,'2014-03-20 05:55:40',1,16,'12','Photo object',3,''),
	(140,'2014-03-20 05:55:40',1,16,'11','Photo object',3,''),
	(141,'2014-03-20 05:55:40',1,16,'10','Photo object',3,''),
	(142,'2014-03-20 05:55:40',1,16,'9','Photo object',3,''),
	(143,'2014-03-20 05:55:40',1,16,'8','Photo object',3,''),
	(144,'2014-03-20 05:55:40',1,16,'7','Photo object',3,''),
	(145,'2014-03-20 05:55:40',1,16,'6','Photo object',3,''),
	(146,'2014-03-20 05:55:40',1,16,'5','Photo object',3,''),
	(147,'2014-03-20 05:55:40',1,16,'4','Photo object',3,''),
	(148,'2014-03-20 05:55:40',1,16,'3','Photo object',3,''),
	(149,'2014-03-20 05:55:40',1,16,'2','Photo object',3,''),
	(150,'2014-03-20 05:55:40',1,16,'1','Photo object',3,''),
	(151,'2014-03-20 05:56:19',1,13,'344','Diary object',3,''),
	(152,'2014-03-20 05:56:19',1,13,'343','Diary object',3,''),
	(153,'2014-03-20 05:56:19',1,13,'342','Diary object',3,''),
	(154,'2014-03-20 05:56:19',1,13,'341','Diary object',3,''),
	(155,'2014-03-20 05:56:19',1,13,'340','Diary object',3,''),
	(156,'2014-03-20 05:56:19',1,13,'339','Diary object',3,''),
	(157,'2014-03-20 05:56:19',1,13,'338','Diary object',3,''),
	(158,'2014-03-20 05:56:19',1,13,'337','Diary object',3,''),
	(159,'2014-03-20 05:56:19',1,13,'336','Diary object',3,''),
	(160,'2014-03-20 05:56:19',1,13,'335','Diary object',3,''),
	(161,'2014-03-20 05:56:19',1,13,'334','Diary object',3,''),
	(162,'2014-03-20 05:56:19',1,13,'333','Diary object',3,''),
	(163,'2014-03-20 05:56:19',1,13,'332','Diary object',3,''),
	(164,'2014-03-20 05:56:19',1,13,'331','Diary object',3,''),
	(165,'2014-03-20 05:56:19',1,13,'330','Diary object',3,''),
	(166,'2014-03-20 05:56:19',1,13,'329','Diary object',3,''),
	(167,'2014-03-20 05:56:19',1,13,'328','Diary object',3,''),
	(168,'2014-03-20 05:56:19',1,13,'327','Diary object',3,''),
	(169,'2014-03-20 05:56:19',1,13,'326','Diary object',3,''),
	(170,'2014-03-20 05:56:19',1,13,'325','Diary object',3,''),
	(171,'2014-03-20 05:56:19',1,13,'324','Diary object',3,''),
	(172,'2014-03-20 05:56:19',1,13,'323','Diary object',3,''),
	(173,'2014-03-20 05:56:19',1,13,'322','Diary object',3,''),
	(174,'2014-03-20 05:56:19',1,13,'321','Diary object',3,''),
	(175,'2014-03-20 05:56:19',1,13,'320','Diary object',3,''),
	(176,'2014-03-20 05:56:19',1,13,'319','Diary object',3,''),
	(177,'2014-03-20 05:56:19',1,13,'318','Diary object',3,''),
	(178,'2014-03-20 05:56:19',1,13,'317','Diary object',3,''),
	(179,'2014-03-20 05:56:19',1,13,'316','Diary object',3,''),
	(180,'2014-03-20 05:56:19',1,13,'315','Diary object',3,''),
	(181,'2014-03-20 05:56:19',1,13,'314','Diary object',3,''),
	(182,'2014-03-20 05:56:19',1,13,'313','Diary object',3,''),
	(183,'2014-03-20 05:56:19',1,13,'312','Diary object',3,''),
	(184,'2014-03-20 05:56:19',1,13,'311','Diary object',3,''),
	(185,'2014-03-20 05:56:19',1,13,'310','Diary object',3,''),
	(186,'2014-03-20 05:56:19',1,13,'309','Diary object',3,''),
	(187,'2014-03-20 05:56:19',1,13,'308','Diary object',3,''),
	(188,'2014-03-20 05:56:19',1,13,'307','Diary object',3,''),
	(189,'2014-03-20 05:56:19',1,13,'306','Diary object',3,''),
	(190,'2014-03-20 05:56:19',1,13,'305','Diary object',3,''),
	(191,'2014-03-20 05:56:19',1,13,'304','Diary object',3,''),
	(192,'2014-03-20 05:56:19',1,13,'303','Diary object',3,''),
	(193,'2014-03-20 05:56:19',1,13,'302','Diary object',3,''),
	(194,'2014-03-20 05:56:19',1,13,'301','Diary object',3,''),
	(195,'2014-03-20 05:56:19',1,13,'300','Diary object',3,''),
	(196,'2014-03-20 05:56:19',1,13,'299','Diary object',3,''),
	(197,'2014-03-20 05:56:19',1,13,'298','Diary object',3,''),
	(198,'2014-03-20 05:56:19',1,13,'297','Diary object',3,''),
	(199,'2014-03-20 05:56:19',1,13,'296','Diary object',3,''),
	(200,'2014-03-20 05:56:19',1,13,'295','Diary object',3,''),
	(201,'2014-03-20 05:56:19',1,13,'294','Diary object',3,''),
	(202,'2014-03-20 05:56:19',1,13,'293','Diary object',3,''),
	(203,'2014-03-20 05:56:19',1,13,'292','Diary object',3,''),
	(204,'2014-03-20 05:56:19',1,13,'291','Diary object',3,''),
	(205,'2014-03-20 05:56:19',1,13,'290','Diary object',3,''),
	(206,'2014-03-20 05:56:19',1,13,'289','Diary object',3,''),
	(207,'2014-03-20 05:56:19',1,13,'288','Diary object',3,''),
	(208,'2014-03-20 05:56:19',1,13,'287','Diary object',3,''),
	(209,'2014-03-20 05:56:19',1,13,'286','Diary object',3,''),
	(210,'2014-03-20 05:56:19',1,13,'285','Diary object',3,''),
	(211,'2014-03-20 05:56:19',1,13,'284','Diary object',3,''),
	(212,'2014-03-20 05:56:19',1,13,'283','Diary object',3,''),
	(213,'2014-03-20 05:56:19',1,13,'282','Diary object',3,''),
	(214,'2014-03-20 05:56:19',1,13,'281','Diary object',3,''),
	(215,'2014-03-20 05:56:19',1,13,'280','Diary object',3,''),
	(216,'2014-03-20 05:56:19',1,13,'279','Diary object',3,''),
	(217,'2014-03-20 05:56:19',1,13,'278','Diary object',3,''),
	(218,'2014-03-20 05:56:19',1,13,'277','Diary object',3,''),
	(219,'2014-03-20 05:56:19',1,13,'276','Diary object',3,''),
	(220,'2014-03-20 05:56:19',1,13,'275','Diary object',3,''),
	(221,'2014-03-20 05:56:19',1,13,'274','Diary object',3,''),
	(222,'2014-03-20 05:56:19',1,13,'273','Diary object',3,''),
	(223,'2014-03-20 05:56:19',1,13,'272','Diary object',3,''),
	(224,'2014-03-20 05:56:19',1,13,'271','Diary object',3,''),
	(225,'2014-03-20 05:56:19',1,13,'270','Diary object',3,''),
	(226,'2014-03-20 05:56:19',1,13,'269','Diary object',3,''),
	(227,'2014-03-20 05:56:19',1,13,'268','Diary object',3,''),
	(228,'2014-03-20 05:56:19',1,13,'267','Diary object',3,''),
	(229,'2014-03-20 05:56:19',1,13,'266','Diary object',3,''),
	(230,'2014-03-20 05:56:19',1,13,'265','Diary object',3,''),
	(231,'2014-03-20 05:56:19',1,13,'264','Diary object',3,''),
	(232,'2014-03-20 05:56:19',1,13,'263','Diary object',3,''),
	(233,'2014-03-20 05:56:19',1,13,'262','Diary object',3,''),
	(234,'2014-03-20 05:56:19',1,13,'261','Diary object',3,''),
	(235,'2014-03-20 05:56:19',1,13,'260','Diary object',3,''),
	(236,'2014-03-20 05:56:19',1,13,'259','Diary object',3,''),
	(237,'2014-03-20 05:56:19',1,13,'258','Diary object',3,''),
	(238,'2014-03-20 05:56:19',1,13,'257','Diary object',3,''),
	(239,'2014-03-20 05:56:19',1,13,'256','Diary object',3,''),
	(240,'2014-03-20 05:56:19',1,13,'255','Diary object',3,''),
	(241,'2014-03-20 05:56:19',1,13,'254','Diary object',3,''),
	(242,'2014-03-20 05:56:19',1,13,'253','Diary object',3,''),
	(243,'2014-03-20 05:56:19',1,13,'252','Diary object',3,''),
	(244,'2014-03-20 05:56:19',1,13,'251','Diary object',3,''),
	(245,'2014-03-20 05:56:19',1,13,'250','Diary object',3,''),
	(246,'2014-03-20 05:56:19',1,13,'249','Diary object',3,''),
	(247,'2014-03-20 05:56:19',1,13,'248','Diary object',3,''),
	(248,'2014-03-20 05:56:19',1,13,'247','Diary object',3,''),
	(249,'2014-03-20 05:56:19',1,13,'246','Diary object',3,''),
	(250,'2014-03-20 05:56:19',1,13,'245','Diary object',3,''),
	(251,'2014-03-20 05:56:29',1,13,'244','Diary object',3,''),
	(252,'2014-03-20 05:56:29',1,13,'243','Diary object',3,''),
	(253,'2014-03-20 05:56:29',1,13,'242','Diary object',3,''),
	(254,'2014-03-20 05:56:29',1,13,'241','Diary object',3,''),
	(255,'2014-03-20 05:56:29',1,13,'240','Diary object',3,''),
	(256,'2014-03-20 05:56:29',1,13,'239','Diary object',3,''),
	(257,'2014-03-20 05:56:29',1,13,'238','Diary object',3,''),
	(258,'2014-03-20 05:56:29',1,13,'237','Diary object',3,''),
	(259,'2014-03-20 05:56:29',1,13,'236','Diary object',3,''),
	(260,'2014-03-20 05:56:29',1,13,'235','Diary object',3,''),
	(261,'2014-03-20 05:56:29',1,13,'234','Diary object',3,''),
	(262,'2014-03-20 05:56:29',1,13,'233','Diary object',3,''),
	(263,'2014-03-20 05:56:29',1,13,'232','Diary object',3,''),
	(264,'2014-03-20 05:56:29',1,13,'231','Diary object',3,''),
	(265,'2014-03-20 05:56:29',1,13,'230','Diary object',3,''),
	(266,'2014-03-20 05:56:29',1,13,'229','Diary object',3,''),
	(267,'2014-03-20 05:56:29',1,13,'228','Diary object',3,''),
	(268,'2014-03-20 05:56:29',1,13,'227','Diary object',3,''),
	(269,'2014-03-20 05:56:29',1,13,'226','Diary object',3,''),
	(270,'2014-03-20 05:56:29',1,13,'225','Diary object',3,''),
	(271,'2014-03-20 05:56:29',1,13,'224','Diary object',3,''),
	(272,'2014-03-20 05:56:29',1,13,'223','Diary object',3,''),
	(273,'2014-03-20 05:56:29',1,13,'222','Diary object',3,''),
	(274,'2014-03-20 05:56:29',1,13,'221','Diary object',3,''),
	(275,'2014-03-20 05:56:29',1,13,'220','Diary object',3,''),
	(276,'2014-03-20 05:56:29',1,13,'219','Diary object',3,''),
	(277,'2014-03-20 05:56:29',1,13,'218','Diary object',3,''),
	(278,'2014-03-20 05:56:29',1,13,'217','Diary object',3,''),
	(279,'2014-03-20 05:56:29',1,13,'216','Diary object',3,''),
	(280,'2014-03-20 05:56:29',1,13,'215','Diary object',3,''),
	(281,'2014-03-20 05:56:29',1,13,'214','Diary object',3,''),
	(282,'2014-03-20 05:56:29',1,13,'213','Diary object',3,''),
	(283,'2014-03-20 05:56:29',1,13,'212','Diary object',3,''),
	(284,'2014-03-20 05:56:29',1,13,'211','Diary object',3,''),
	(285,'2014-03-20 05:56:29',1,13,'210','Diary object',3,''),
	(286,'2014-03-20 05:56:29',1,13,'209','Diary object',3,''),
	(287,'2014-03-20 05:56:29',1,13,'208','Diary object',3,''),
	(288,'2014-03-20 05:56:29',1,13,'207','Diary object',3,''),
	(289,'2014-03-20 05:56:29',1,13,'206','Diary object',3,''),
	(290,'2014-03-20 05:56:29',1,13,'205','Diary object',3,''),
	(291,'2014-03-20 05:56:29',1,13,'204','Diary object',3,''),
	(292,'2014-03-20 05:56:29',1,13,'203','Diary object',3,''),
	(293,'2014-03-20 05:56:29',1,13,'202','Diary object',3,''),
	(294,'2014-03-20 05:56:29',1,13,'201','Diary object',3,''),
	(295,'2014-03-20 05:56:29',1,13,'200','Diary object',3,''),
	(296,'2014-03-20 05:56:29',1,13,'199','Diary object',3,''),
	(297,'2014-03-20 05:56:29',1,13,'198','Diary object',3,''),
	(298,'2014-03-20 05:56:29',1,13,'197','Diary object',3,''),
	(299,'2014-03-20 05:56:29',1,13,'196','Diary object',3,''),
	(300,'2014-03-20 05:56:29',1,13,'195','Diary object',3,''),
	(301,'2014-03-20 05:56:29',1,13,'194','Diary object',3,''),
	(302,'2014-03-20 05:56:29',1,13,'193','Diary object',3,''),
	(303,'2014-03-20 05:56:29',1,13,'192','Diary object',3,''),
	(304,'2014-03-20 05:56:29',1,13,'191','Diary object',3,''),
	(305,'2014-03-20 05:56:29',1,13,'190','Diary object',3,''),
	(306,'2014-03-20 05:56:29',1,13,'189','Diary object',3,''),
	(307,'2014-03-20 05:56:29',1,13,'188','Diary object',3,''),
	(308,'2014-03-20 05:56:29',1,13,'187','Diary object',3,''),
	(309,'2014-03-20 05:56:29',1,13,'186','Diary object',3,''),
	(310,'2014-03-20 05:56:29',1,13,'185','Diary object',3,''),
	(311,'2014-03-20 05:56:29',1,13,'184','Diary object',3,''),
	(312,'2014-03-20 05:56:29',1,13,'183','Diary object',3,''),
	(313,'2014-03-20 05:56:29',1,13,'182','Diary object',3,''),
	(314,'2014-03-20 05:56:29',1,13,'181','Diary object',3,''),
	(315,'2014-03-20 05:56:29',1,13,'180','Diary object',3,''),
	(316,'2014-03-20 05:56:29',1,13,'179','Diary object',3,''),
	(317,'2014-03-20 05:56:29',1,13,'178','Diary object',3,''),
	(318,'2014-03-20 05:56:29',1,13,'177','Diary object',3,''),
	(319,'2014-03-20 05:56:29',1,13,'176','Diary object',3,''),
	(320,'2014-03-20 05:56:29',1,13,'175','Diary object',3,''),
	(321,'2014-03-20 05:56:29',1,13,'174','Diary object',3,''),
	(322,'2014-03-20 05:56:29',1,13,'173','Diary object',3,''),
	(323,'2014-03-20 05:56:29',1,13,'172','Diary object',3,''),
	(324,'2014-03-20 05:56:29',1,13,'171','Diary object',3,''),
	(325,'2014-03-20 05:56:29',1,13,'170','Diary object',3,''),
	(326,'2014-03-20 05:56:29',1,13,'169','Diary object',3,''),
	(327,'2014-03-20 05:56:29',1,13,'168','Diary object',3,''),
	(328,'2014-03-20 05:56:29',1,13,'167','Diary object',3,''),
	(329,'2014-03-20 05:56:29',1,13,'166','Diary object',3,''),
	(330,'2014-03-20 05:56:29',1,13,'165','Diary object',3,''),
	(331,'2014-03-20 05:56:29',1,13,'164','Diary object',3,''),
	(332,'2014-03-20 05:56:29',1,13,'163','Diary object',3,''),
	(333,'2014-03-20 05:56:29',1,13,'162','Diary object',3,''),
	(334,'2014-03-20 05:56:29',1,13,'161','Diary object',3,''),
	(335,'2014-03-20 05:56:29',1,13,'160','Diary object',3,''),
	(336,'2014-03-20 05:56:29',1,13,'159','Diary object',3,''),
	(337,'2014-03-20 05:56:29',1,13,'158','Diary object',3,''),
	(338,'2014-03-20 05:56:29',1,13,'157','Diary object',3,''),
	(339,'2014-03-20 05:56:29',1,13,'156','Diary object',3,''),
	(340,'2014-03-20 05:56:29',1,13,'155','Diary object',3,''),
	(341,'2014-03-20 05:56:29',1,13,'154','Diary object',3,''),
	(342,'2014-03-20 05:56:29',1,13,'153','Diary object',3,''),
	(343,'2014-03-20 05:56:29',1,13,'152','Diary object',3,''),
	(344,'2014-03-20 05:56:29',1,13,'151','Diary object',3,''),
	(345,'2014-03-20 05:56:29',1,13,'150','Diary object',3,''),
	(346,'2014-03-20 05:56:29',1,13,'149','Diary object',3,''),
	(347,'2014-03-20 05:56:29',1,13,'148','Diary object',3,''),
	(348,'2014-03-20 05:56:29',1,13,'147','Diary object',3,''),
	(349,'2014-03-20 05:56:29',1,13,'146','Diary object',3,''),
	(350,'2014-03-20 05:56:29',1,13,'145','Diary object',3,''),
	(351,'2014-03-20 05:56:37',1,13,'144','Diary object',3,''),
	(352,'2014-03-20 05:56:37',1,13,'143','Diary object',3,''),
	(353,'2014-03-20 05:56:37',1,13,'142','Diary object',3,''),
	(354,'2014-03-20 05:56:37',1,13,'141','Diary object',3,''),
	(355,'2014-03-20 05:56:37',1,13,'140','Diary object',3,''),
	(356,'2014-03-20 05:56:37',1,13,'139','Diary object',3,''),
	(357,'2014-03-20 05:56:37',1,13,'138','Diary object',3,''),
	(358,'2014-03-20 05:56:37',1,13,'137','Diary object',3,''),
	(359,'2014-03-20 05:56:37',1,13,'136','Diary object',3,''),
	(360,'2014-03-20 05:56:37',1,13,'135','Diary object',3,''),
	(361,'2014-03-20 05:56:37',1,13,'134','Diary object',3,''),
	(362,'2014-03-20 05:56:37',1,13,'133','Diary object',3,''),
	(363,'2014-03-20 05:56:37',1,13,'132','Diary object',3,''),
	(364,'2014-03-20 05:56:37',1,13,'131','Diary object',3,''),
	(365,'2014-03-20 05:56:37',1,13,'130','Diary object',3,''),
	(366,'2014-03-20 05:56:37',1,13,'129','Diary object',3,''),
	(367,'2014-03-20 05:56:37',1,13,'128','Diary object',3,''),
	(368,'2014-03-20 05:56:37',1,13,'127','Diary object',3,''),
	(369,'2014-03-20 05:56:37',1,13,'126','Diary object',3,''),
	(370,'2014-03-20 05:56:37',1,13,'125','Diary object',3,''),
	(371,'2014-03-20 05:56:37',1,13,'124','Diary object',3,''),
	(372,'2014-03-20 05:56:37',1,13,'123','Diary object',3,''),
	(373,'2014-03-20 05:56:37',1,13,'122','Diary object',3,''),
	(374,'2014-03-20 05:56:37',1,13,'121','Diary object',3,''),
	(375,'2014-03-20 05:56:37',1,13,'120','Diary object',3,''),
	(376,'2014-03-20 05:56:37',1,13,'119','Diary object',3,''),
	(377,'2014-03-20 05:56:37',1,13,'118','Diary object',3,''),
	(378,'2014-03-20 05:56:37',1,13,'117','Diary object',3,''),
	(379,'2014-03-20 05:56:37',1,13,'116','Diary object',3,''),
	(380,'2014-03-20 05:56:37',1,13,'115','Diary object',3,''),
	(381,'2014-03-20 05:56:37',1,13,'114','Diary object',3,''),
	(382,'2014-03-20 05:56:37',1,13,'113','Diary object',3,''),
	(383,'2014-03-20 05:56:37',1,13,'112','Diary object',3,''),
	(384,'2014-03-20 05:56:37',1,13,'111','Diary object',3,''),
	(385,'2014-03-20 05:56:37',1,13,'110','Diary object',3,''),
	(386,'2014-03-20 05:56:37',1,13,'109','Diary object',3,''),
	(387,'2014-03-20 05:56:37',1,13,'108','Diary object',3,''),
	(388,'2014-03-20 05:56:37',1,13,'107','Diary object',3,''),
	(389,'2014-03-20 05:56:37',1,13,'106','Diary object',3,''),
	(390,'2014-03-20 05:56:37',1,13,'105','Diary object',3,''),
	(391,'2014-03-20 05:56:37',1,13,'104','Diary object',3,''),
	(392,'2014-03-20 05:56:37',1,13,'103','Diary object',3,''),
	(393,'2014-03-20 05:56:37',1,13,'102','Diary object',3,''),
	(394,'2014-03-20 05:56:37',1,13,'101','Diary object',3,''),
	(395,'2014-03-20 05:56:37',1,13,'100','Diary object',3,''),
	(396,'2014-03-20 05:56:37',1,13,'99','Diary object',3,''),
	(397,'2014-03-20 05:56:37',1,13,'98','Diary object',3,''),
	(398,'2014-03-20 05:56:37',1,13,'97','Diary object',3,''),
	(399,'2014-03-20 05:56:37',1,13,'96','Diary object',3,''),
	(400,'2014-03-20 05:56:37',1,13,'95','Diary object',3,''),
	(401,'2014-03-20 05:56:37',1,13,'94','Diary object',3,''),
	(402,'2014-03-20 05:56:37',1,13,'93','Diary object',3,''),
	(403,'2014-03-20 05:56:37',1,13,'92','Diary object',3,''),
	(404,'2014-03-20 05:56:37',1,13,'91','Diary object',3,''),
	(405,'2014-03-20 05:56:37',1,13,'90','Diary object',3,''),
	(406,'2014-03-20 05:56:37',1,13,'89','Diary object',3,''),
	(407,'2014-03-20 05:56:37',1,13,'88','Diary object',3,''),
	(408,'2014-03-20 05:56:37',1,13,'87','Diary object',3,''),
	(409,'2014-03-20 05:56:37',1,13,'86','Diary object',3,''),
	(410,'2014-03-20 05:56:37',1,13,'85','Diary object',3,''),
	(411,'2014-03-20 05:56:37',1,13,'84','Diary object',3,''),
	(412,'2014-03-20 05:56:37',1,13,'83','Diary object',3,''),
	(413,'2014-03-20 05:56:37',1,13,'82','Diary object',3,''),
	(414,'2014-03-20 05:56:37',1,13,'81','Diary object',3,''),
	(415,'2014-03-20 05:56:37',1,13,'80','Diary object',3,''),
	(416,'2014-03-20 05:56:37',1,13,'79','Diary object',3,''),
	(417,'2014-03-20 05:56:37',1,13,'78','Diary object',3,''),
	(418,'2014-03-20 05:56:37',1,13,'77','Diary object',3,''),
	(419,'2014-03-20 05:56:37',1,13,'76','Diary object',3,''),
	(420,'2014-03-20 05:56:37',1,13,'75','Diary object',3,''),
	(421,'2014-03-20 05:56:37',1,13,'74','Diary object',3,''),
	(422,'2014-03-20 05:56:37',1,13,'73','Diary object',3,''),
	(423,'2014-03-20 05:56:37',1,13,'72','Diary object',3,''),
	(424,'2014-03-20 05:56:37',1,13,'71','Diary object',3,''),
	(425,'2014-03-20 05:56:37',1,13,'70','Diary object',3,''),
	(426,'2014-03-20 05:56:37',1,13,'69','Diary object',3,''),
	(427,'2014-03-20 05:56:37',1,13,'68','Diary object',3,''),
	(428,'2014-03-20 05:56:37',1,13,'67','Diary object',3,''),
	(429,'2014-03-20 05:56:37',1,13,'66','Diary object',3,''),
	(430,'2014-03-20 05:56:37',1,13,'65','Diary object',3,''),
	(431,'2014-03-20 05:56:37',1,13,'64','Diary object',3,''),
	(432,'2014-03-20 05:56:37',1,13,'63','Diary object',3,''),
	(433,'2014-03-20 05:56:37',1,13,'62','Diary object',3,''),
	(434,'2014-03-20 05:56:37',1,13,'61','Diary object',3,''),
	(435,'2014-03-20 05:56:37',1,13,'60','Diary object',3,''),
	(436,'2014-03-20 05:56:37',1,13,'59','Diary object',3,''),
	(437,'2014-03-20 05:56:37',1,13,'58','Diary object',3,''),
	(438,'2014-03-20 05:56:37',1,13,'57','Diary object',3,''),
	(439,'2014-03-20 05:56:37',1,13,'56','Diary object',3,''),
	(440,'2014-03-20 05:56:37',1,13,'55','Diary object',3,''),
	(441,'2014-03-20 05:56:37',1,13,'54','Diary object',3,''),
	(442,'2014-03-20 05:56:37',1,13,'53','Diary object',3,''),
	(443,'2014-03-20 05:56:37',1,13,'52','Diary object',3,''),
	(444,'2014-03-20 05:56:37',1,13,'51','Diary object',3,''),
	(445,'2014-03-20 05:56:37',1,13,'50','Diary object',3,''),
	(446,'2014-03-20 05:56:37',1,13,'49','Diary object',3,''),
	(447,'2014-03-20 05:56:37',1,13,'48','Diary object',3,''),
	(448,'2014-03-20 05:56:37',1,13,'47','Diary object',3,''),
	(449,'2014-03-20 05:56:37',1,13,'46','Diary object',3,''),
	(450,'2014-03-20 05:56:37',1,13,'45','Diary object',3,''),
	(451,'2014-03-20 05:56:44',1,13,'44','Diary object',3,''),
	(452,'2014-03-20 05:56:44',1,13,'43','Diary object',3,''),
	(453,'2014-03-20 05:56:44',1,13,'42','Diary object',3,''),
	(454,'2014-03-20 05:56:44',1,13,'41','Diary object',3,''),
	(455,'2014-03-20 05:56:44',1,13,'40','Diary object',3,''),
	(456,'2014-03-20 05:56:44',1,13,'39','Diary object',3,''),
	(457,'2014-03-20 05:56:44',1,13,'38','Diary object',3,''),
	(458,'2014-03-20 05:56:44',1,13,'37','Diary object',3,''),
	(459,'2014-03-20 05:56:44',1,13,'36','Diary object',3,''),
	(460,'2014-03-20 05:56:44',1,13,'35','Diary object',3,''),
	(461,'2014-03-20 05:56:44',1,13,'34','Diary object',3,''),
	(462,'2014-03-20 05:56:44',1,13,'33','Diary object',3,''),
	(463,'2014-03-20 05:56:44',1,13,'32','Diary object',3,''),
	(464,'2014-03-20 05:56:44',1,13,'31','Diary object',3,''),
	(465,'2014-03-20 05:56:44',1,13,'30','Diary object',3,''),
	(466,'2014-03-20 05:56:44',1,13,'29','Diary object',3,''),
	(467,'2014-03-20 05:56:44',1,13,'28','Diary object',3,''),
	(468,'2014-03-20 05:56:44',1,13,'27','Diary object',3,''),
	(469,'2014-03-20 05:56:44',1,13,'26','Diary object',3,''),
	(470,'2014-03-20 05:56:44',1,13,'25','Diary object',3,''),
	(471,'2014-03-20 05:56:44',1,13,'24','Diary object',3,''),
	(472,'2014-03-20 05:56:44',1,13,'23','Diary object',3,''),
	(473,'2014-03-20 05:56:44',1,13,'22','Diary object',3,''),
	(474,'2014-03-20 05:56:44',1,13,'21','Diary object',3,''),
	(475,'2014-03-20 05:56:44',1,13,'20','Diary object',3,''),
	(476,'2014-03-20 05:56:44',1,13,'19','Diary object',3,''),
	(477,'2014-03-20 05:56:44',1,13,'18','Diary object',3,''),
	(478,'2014-03-20 05:56:44',1,13,'17','Diary object',3,''),
	(479,'2014-03-20 05:56:44',1,13,'16','Diary object',3,''),
	(480,'2014-03-20 05:56:44',1,13,'15','Diary object',3,''),
	(481,'2014-03-20 05:56:44',1,13,'14','Diary object',3,''),
	(482,'2014-03-20 05:56:44',1,13,'13','Diary object',3,''),
	(483,'2014-03-20 05:56:44',1,13,'12','Diary object',3,''),
	(484,'2014-03-20 05:56:44',1,13,'11','Diary object',3,''),
	(485,'2014-03-20 05:56:44',1,13,'10','Diary object',3,''),
	(486,'2014-03-20 05:56:44',1,13,'9','Diary object',3,''),
	(487,'2014-03-20 05:56:44',1,13,'8','Diary object',3,''),
	(488,'2014-03-20 05:56:44',1,13,'7','Diary object',3,''),
	(489,'2014-03-20 05:56:44',1,13,'6','Diary object',3,''),
	(490,'2014-03-20 05:56:44',1,13,'5','Diary object',3,''),
	(491,'2014-03-20 05:56:44',1,13,'4','Diary object',3,''),
	(492,'2014-03-20 05:56:44',1,13,'3','Diary object',3,''),
	(493,'2014-03-20 05:56:44',1,13,'2','Diary object',3,''),
	(494,'2014-03-20 05:56:44',1,13,'1','Diary object',3,''),
	(495,'2014-03-25 02:34:28',1,13,'556','Diary object',3,''),
	(496,'2014-03-25 02:34:29',1,13,'555','Diary object',3,''),
	(497,'2014-03-25 02:34:29',1,13,'554','Diary object',3,''),
	(498,'2014-03-25 02:34:29',1,13,'553','Diary object',3,''),
	(499,'2014-03-25 02:34:29',1,13,'552','Diary object',3,''),
	(500,'2014-03-25 02:34:29',1,13,'551','Diary object',3,''),
	(501,'2014-03-25 02:34:29',1,13,'550','Diary object',3,''),
	(502,'2014-03-25 02:34:29',1,13,'549','Diary object',3,''),
	(503,'2014-03-25 02:34:29',1,13,'548','Diary object',3,''),
	(504,'2014-03-25 02:34:29',1,13,'547','Diary object',3,''),
	(505,'2014-03-25 02:34:29',1,13,'546','Diary object',3,''),
	(506,'2014-03-25 02:34:29',1,13,'545','Diary object',3,''),
	(507,'2014-03-25 02:34:29',1,13,'544','Diary object',3,''),
	(508,'2014-03-25 02:34:29',1,13,'543','Diary object',3,''),
	(509,'2014-03-25 02:34:29',1,13,'542','Diary object',3,''),
	(510,'2014-03-25 02:34:29',1,13,'541','Diary object',3,''),
	(511,'2014-03-25 02:34:29',1,13,'540','Diary object',3,''),
	(512,'2014-03-25 02:34:29',1,13,'539','Diary object',3,''),
	(513,'2014-03-25 02:34:29',1,13,'538','Diary object',3,''),
	(514,'2014-03-25 02:34:29',1,13,'537','Diary object',3,''),
	(515,'2014-03-25 02:34:29',1,13,'536','Diary object',3,''),
	(516,'2014-03-25 02:34:29',1,13,'535','Diary object',3,''),
	(517,'2014-03-25 02:34:29',1,13,'534','Diary object',3,''),
	(518,'2014-03-25 02:34:29',1,13,'533','Diary object',3,''),
	(519,'2014-03-25 02:34:29',1,13,'532','Diary object',3,''),
	(520,'2014-03-25 02:34:29',1,13,'531','Diary object',3,''),
	(521,'2014-03-25 02:34:29',1,13,'530','Diary object',3,''),
	(522,'2014-03-25 02:34:29',1,13,'529','Diary object',3,''),
	(523,'2014-03-25 02:34:29',1,13,'528','Diary object',3,''),
	(524,'2014-03-25 02:34:29',1,13,'527','Diary object',3,''),
	(525,'2014-03-25 02:34:29',1,13,'526','Diary object',3,''),
	(526,'2014-03-25 02:34:29',1,13,'525','Diary object',3,''),
	(527,'2014-03-25 02:34:29',1,13,'524','Diary object',3,''),
	(528,'2014-03-25 02:34:29',1,13,'523','Diary object',3,''),
	(529,'2014-03-25 02:34:29',1,13,'522','Diary object',3,''),
	(530,'2014-03-25 02:34:29',1,13,'521','Diary object',3,''),
	(531,'2014-03-25 02:34:29',1,13,'520','Diary object',3,''),
	(532,'2014-03-25 02:34:29',1,13,'519','Diary object',3,''),
	(533,'2014-03-25 02:34:29',1,13,'518','Diary object',3,''),
	(534,'2014-03-25 02:34:29',1,13,'517','Diary object',3,''),
	(535,'2014-03-25 02:34:29',1,13,'516','Diary object',3,''),
	(536,'2014-03-25 02:34:29',1,13,'515','Diary object',3,''),
	(537,'2014-03-25 02:34:29',1,13,'514','Diary object',3,''),
	(538,'2014-03-25 02:34:29',1,13,'513','Diary object',3,''),
	(539,'2014-03-25 02:34:29',1,13,'512','Diary object',3,''),
	(540,'2014-03-25 02:34:29',1,13,'511','Diary object',3,''),
	(541,'2014-03-25 02:34:29',1,13,'510','Diary object',3,''),
	(542,'2014-03-25 02:34:29',1,13,'509','Diary object',3,''),
	(543,'2014-03-25 02:34:29',1,13,'508','Diary object',3,''),
	(544,'2014-03-25 02:34:29',1,13,'507','Diary object',3,''),
	(545,'2014-03-25 02:34:29',1,13,'506','Diary object',3,''),
	(546,'2014-03-25 02:34:29',1,13,'505','Diary object',3,''),
	(547,'2014-03-25 02:34:29',1,13,'504','Diary object',3,''),
	(548,'2014-03-25 02:34:29',1,13,'503','Diary object',3,''),
	(549,'2014-03-25 02:34:29',1,13,'502','Diary object',3,''),
	(550,'2014-03-25 02:34:29',1,13,'501','Diary object',3,''),
	(551,'2014-03-25 02:34:29',1,13,'500','Diary object',3,''),
	(552,'2014-03-25 02:34:29',1,13,'499','Diary object',3,''),
	(553,'2014-03-25 02:34:29',1,13,'498','Diary object',3,''),
	(554,'2014-03-25 02:34:29',1,13,'497','Diary object',3,''),
	(555,'2014-03-25 02:34:29',1,13,'496','Diary object',3,''),
	(556,'2014-03-25 02:34:29',1,13,'495','Diary object',3,''),
	(557,'2014-03-25 02:34:29',1,13,'494','Diary object',3,''),
	(558,'2014-03-25 02:34:29',1,13,'493','Diary object',3,''),
	(559,'2014-03-25 02:34:29',1,13,'492','Diary object',3,''),
	(560,'2014-03-25 02:34:29',1,13,'491','Diary object',3,''),
	(561,'2014-03-25 02:34:29',1,13,'490','Diary object',3,''),
	(562,'2014-03-25 02:34:29',1,13,'489','Diary object',3,''),
	(563,'2014-03-25 02:34:29',1,13,'488','Diary object',3,''),
	(564,'2014-03-25 02:34:29',1,13,'487','Diary object',3,''),
	(565,'2014-03-25 02:34:29',1,13,'486','Diary object',3,''),
	(566,'2014-03-25 02:34:29',1,13,'485','Diary object',3,''),
	(567,'2014-03-25 02:34:29',1,13,'484','Diary object',3,''),
	(568,'2014-03-25 02:34:29',1,13,'483','Diary object',3,''),
	(569,'2014-03-25 02:34:29',1,13,'482','Diary object',3,''),
	(570,'2014-03-25 02:34:29',1,13,'481','Diary object',3,''),
	(571,'2014-03-25 02:34:29',1,13,'480','Diary object',3,''),
	(572,'2014-03-25 02:34:29',1,13,'479','Diary object',3,''),
	(573,'2014-03-25 02:34:29',1,13,'478','Diary object',3,''),
	(574,'2014-03-25 02:34:29',1,13,'477','Diary object',3,''),
	(575,'2014-03-25 02:34:29',1,13,'476','Diary object',3,''),
	(576,'2014-03-25 02:34:29',1,13,'475','Diary object',3,''),
	(577,'2014-03-25 02:34:29',1,13,'474','Diary object',3,''),
	(578,'2014-03-25 02:34:29',1,13,'473','Diary object',3,''),
	(579,'2014-03-25 02:34:29',1,13,'472','Diary object',3,''),
	(580,'2014-03-25 02:34:29',1,13,'471','Diary object',3,''),
	(581,'2014-03-25 02:34:29',1,13,'470','Diary object',3,''),
	(582,'2014-03-25 02:34:29',1,13,'469','Diary object',3,''),
	(583,'2014-03-25 02:34:29',1,13,'468','Diary object',3,''),
	(584,'2014-03-25 02:34:29',1,13,'467','Diary object',3,''),
	(585,'2014-03-25 02:34:29',1,13,'466','Diary object',3,''),
	(586,'2014-03-25 02:34:29',1,13,'465','Diary object',3,''),
	(587,'2014-03-25 02:34:29',1,13,'464','Diary object',3,''),
	(588,'2014-03-25 02:34:29',1,13,'463','Diary object',3,''),
	(589,'2014-03-25 02:34:29',1,13,'462','Diary object',3,''),
	(590,'2014-03-25 02:34:29',1,13,'461','Diary object',3,''),
	(591,'2014-03-25 02:34:29',1,13,'460','Diary object',3,''),
	(592,'2014-03-25 02:34:29',1,13,'459','Diary object',3,''),
	(593,'2014-03-25 02:34:29',1,13,'458','Diary object',3,''),
	(594,'2014-03-25 02:34:29',1,13,'457','Diary object',3,''),
	(595,'2014-03-25 02:34:37',1,13,'456','Diary object',3,''),
	(596,'2014-03-25 02:34:37',1,13,'455','Diary object',3,''),
	(597,'2014-03-25 02:34:37',1,13,'454','Diary object',3,''),
	(598,'2014-03-25 02:34:37',1,13,'453','Diary object',3,''),
	(599,'2014-03-25 02:34:37',1,13,'452','Diary object',3,''),
	(600,'2014-03-25 02:34:37',1,13,'451','Diary object',3,''),
	(601,'2014-03-25 02:34:37',1,13,'450','Diary object',3,''),
	(602,'2014-03-25 02:34:37',1,13,'449','Diary object',3,''),
	(603,'2014-03-25 02:34:37',1,13,'448','Diary object',3,''),
	(604,'2014-03-25 02:34:37',1,13,'447','Diary object',3,''),
	(605,'2014-03-25 02:34:37',1,13,'446','Diary object',3,''),
	(606,'2014-03-25 02:34:37',1,13,'445','Diary object',3,''),
	(607,'2014-03-25 02:34:37',1,13,'444','Diary object',3,''),
	(608,'2014-03-25 02:34:37',1,13,'443','Diary object',3,''),
	(609,'2014-03-25 02:34:37',1,13,'442','Diary object',3,''),
	(610,'2014-03-25 02:34:37',1,13,'441','Diary object',3,''),
	(611,'2014-03-25 02:34:37',1,13,'440','Diary object',3,''),
	(612,'2014-03-25 02:34:37',1,13,'439','Diary object',3,''),
	(613,'2014-03-25 02:34:37',1,13,'438','Diary object',3,''),
	(614,'2014-03-25 02:34:37',1,13,'437','Diary object',3,''),
	(615,'2014-03-25 02:34:37',1,13,'436','Diary object',3,''),
	(616,'2014-03-25 02:34:37',1,13,'435','Diary object',3,''),
	(617,'2014-03-25 02:34:37',1,13,'434','Diary object',3,''),
	(618,'2014-03-25 02:34:37',1,13,'433','Diary object',3,''),
	(619,'2014-03-25 02:34:37',1,13,'432','Diary object',3,''),
	(620,'2014-03-25 02:34:37',1,13,'431','Diary object',3,''),
	(621,'2014-03-25 02:34:37',1,13,'430','Diary object',3,''),
	(622,'2014-03-25 02:34:37',1,13,'429','Diary object',3,''),
	(623,'2014-03-25 02:34:37',1,13,'428','Diary object',3,''),
	(624,'2014-03-25 02:34:37',1,13,'427','Diary object',3,''),
	(625,'2014-03-25 02:34:37',1,13,'426','Diary object',3,''),
	(626,'2014-03-25 02:34:37',1,13,'425','Diary object',3,''),
	(627,'2014-03-25 02:34:37',1,13,'424','Diary object',3,''),
	(628,'2014-03-25 02:34:37',1,13,'423','Diary object',3,''),
	(629,'2014-03-25 02:34:37',1,13,'422','Diary object',3,''),
	(630,'2014-03-25 02:34:37',1,13,'421','Diary object',3,''),
	(631,'2014-03-25 02:34:37',1,13,'420','Diary object',3,''),
	(632,'2014-03-25 02:34:37',1,13,'419','Diary object',3,''),
	(633,'2014-03-25 02:34:37',1,13,'418','Diary object',3,''),
	(634,'2014-03-25 02:34:37',1,13,'417','Diary object',3,''),
	(635,'2014-03-25 02:34:37',1,13,'416','Diary object',3,''),
	(636,'2014-03-25 02:34:37',1,13,'415','Diary object',3,''),
	(637,'2014-03-25 02:34:37',1,13,'414','Diary object',3,''),
	(638,'2014-03-25 02:34:37',1,13,'413','Diary object',3,''),
	(639,'2014-03-25 02:34:37',1,13,'412','Diary object',3,''),
	(640,'2014-03-25 02:34:37',1,13,'411','Diary object',3,''),
	(641,'2014-03-25 02:34:37',1,13,'410','Diary object',3,''),
	(642,'2014-03-25 02:34:37',1,13,'409','Diary object',3,''),
	(643,'2014-03-25 02:34:37',1,13,'408','Diary object',3,''),
	(644,'2014-03-25 02:34:37',1,13,'407','Diary object',3,''),
	(645,'2014-03-25 02:34:37',1,13,'406','Diary object',3,''),
	(646,'2014-03-25 02:34:37',1,13,'405','Diary object',3,''),
	(647,'2014-03-25 02:34:37',1,13,'404','Diary object',3,''),
	(648,'2014-03-25 02:34:37',1,13,'403','Diary object',3,''),
	(649,'2014-03-25 02:34:37',1,13,'402','Diary object',3,''),
	(650,'2014-03-25 02:34:37',1,13,'401','Diary object',3,''),
	(651,'2014-03-25 02:34:37',1,13,'400','Diary object',3,''),
	(652,'2014-03-25 02:34:37',1,13,'399','Diary object',3,''),
	(653,'2014-03-25 02:34:37',1,13,'398','Diary object',3,''),
	(654,'2014-03-25 02:34:37',1,13,'397','Diary object',3,''),
	(655,'2014-03-25 02:34:37',1,13,'396','Diary object',3,''),
	(656,'2014-03-25 02:34:37',1,13,'395','Diary object',3,''),
	(657,'2014-03-25 02:34:37',1,13,'394','Diary object',3,''),
	(658,'2014-03-25 02:34:37',1,13,'393','Diary object',3,''),
	(659,'2014-03-25 02:34:37',1,13,'392','Diary object',3,''),
	(660,'2014-03-25 02:34:37',1,13,'391','Diary object',3,''),
	(661,'2014-03-25 02:34:37',1,13,'390','Diary object',3,''),
	(662,'2014-03-25 02:34:37',1,13,'389','Diary object',3,''),
	(663,'2014-03-25 02:34:37',1,13,'388','Diary object',3,''),
	(664,'2014-03-25 02:34:37',1,13,'387','Diary object',3,''),
	(665,'2014-03-25 02:34:37',1,13,'386','Diary object',3,''),
	(666,'2014-03-25 02:34:37',1,13,'385','Diary object',3,''),
	(667,'2014-03-25 02:34:37',1,13,'384','Diary object',3,''),
	(668,'2014-03-25 02:34:37',1,13,'383','Diary object',3,''),
	(669,'2014-03-25 02:34:37',1,13,'382','Diary object',3,''),
	(670,'2014-03-25 02:34:37',1,13,'381','Diary object',3,''),
	(671,'2014-03-25 02:34:37',1,13,'380','Diary object',3,''),
	(672,'2014-03-25 02:34:37',1,13,'379','Diary object',3,''),
	(673,'2014-03-25 02:34:37',1,13,'378','Diary object',3,''),
	(674,'2014-03-25 02:34:37',1,13,'377','Diary object',3,''),
	(675,'2014-03-25 02:34:37',1,13,'376','Diary object',3,''),
	(676,'2014-03-25 02:34:37',1,13,'375','Diary object',3,''),
	(677,'2014-03-25 02:34:37',1,13,'374','Diary object',3,''),
	(678,'2014-03-25 02:34:37',1,13,'373','Diary object',3,''),
	(679,'2014-03-25 02:34:37',1,13,'372','Diary object',3,''),
	(680,'2014-03-25 02:34:37',1,13,'371','Diary object',3,''),
	(681,'2014-03-25 02:34:37',1,13,'370','Diary object',3,''),
	(682,'2014-03-25 02:34:37',1,13,'369','Diary object',3,''),
	(683,'2014-03-25 02:34:37',1,13,'368','Diary object',3,''),
	(684,'2014-03-25 02:34:37',1,13,'367','Diary object',3,''),
	(685,'2014-03-25 02:34:37',1,13,'366','Diary object',3,''),
	(686,'2014-03-25 02:34:37',1,13,'365','Diary object',3,''),
	(687,'2014-03-25 02:34:37',1,13,'364','Diary object',3,''),
	(688,'2014-03-25 02:34:37',1,13,'363','Diary object',3,''),
	(689,'2014-03-25 02:34:37',1,13,'362','Diary object',3,''),
	(690,'2014-03-25 02:34:37',1,13,'361','Diary object',3,''),
	(691,'2014-03-25 02:34:37',1,13,'360','Diary object',3,''),
	(692,'2014-03-25 02:34:37',1,13,'359','Diary object',3,''),
	(693,'2014-03-25 02:34:37',1,13,'358','Diary object',3,''),
	(694,'2014-03-25 02:34:37',1,13,'357','Diary object',3,''),
	(695,'2014-03-25 02:34:43',1,13,'356','Diary object',3,''),
	(696,'2014-03-25 02:34:43',1,13,'355','Diary object',3,''),
	(697,'2014-03-25 02:34:43',1,13,'354','Diary object',3,''),
	(698,'2014-03-25 02:34:43',1,13,'353','Diary object',3,''),
	(699,'2014-03-25 02:34:43',1,13,'352','Diary object',3,''),
	(700,'2014-03-25 02:34:43',1,13,'351','Diary object',3,''),
	(701,'2014-03-25 02:34:43',1,13,'350','Diary object',3,''),
	(702,'2014-03-25 02:34:43',1,13,'349','Diary object',3,''),
	(703,'2014-03-25 02:34:43',1,13,'348','Diary object',3,''),
	(704,'2014-03-25 02:34:43',1,13,'347','Diary object',3,''),
	(705,'2014-03-25 02:34:43',1,13,'346','Diary object',3,''),
	(706,'2014-03-25 02:34:43',1,13,'345','Diary object',3,''),
	(707,'2014-03-27 06:45:48',1,13,'693','Diary object',3,''),
	(708,'2014-03-27 06:45:49',1,13,'692','Diary object',3,''),
	(709,'2014-03-27 06:45:49',1,13,'691','Diary object',3,''),
	(710,'2014-03-27 06:45:49',1,13,'690','Diary object',3,''),
	(711,'2014-03-27 06:45:49',1,13,'689','Diary object',3,''),
	(712,'2014-03-27 06:45:49',1,13,'688','Diary object',3,''),
	(713,'2014-03-27 06:45:49',1,13,'687','Diary object',3,''),
	(714,'2014-03-27 06:45:49',1,13,'686','Diary object',3,''),
	(715,'2014-03-27 06:45:49',1,13,'685','Diary object',3,''),
	(716,'2014-03-27 06:45:49',1,13,'684','Diary object',3,''),
	(717,'2014-03-27 06:45:49',1,13,'683','Diary object',3,''),
	(718,'2014-03-27 06:45:49',1,13,'682','Diary object',3,''),
	(719,'2014-03-27 06:45:49',1,13,'681','Diary object',3,''),
	(720,'2014-03-27 06:45:49',1,13,'680','Diary object',3,''),
	(721,'2014-03-27 06:45:49',1,13,'679','Diary object',3,''),
	(722,'2014-03-27 06:45:49',1,13,'678','Diary object',3,''),
	(723,'2014-03-27 06:45:49',1,13,'677','Diary object',3,''),
	(724,'2014-03-27 06:45:49',1,13,'676','Diary object',3,''),
	(725,'2014-03-27 06:45:49',1,13,'675','Diary object',3,''),
	(726,'2014-03-27 06:45:49',1,13,'674','Diary object',3,''),
	(727,'2014-03-27 06:45:49',1,13,'673','Diary object',3,''),
	(728,'2014-03-27 06:45:49',1,13,'672','Diary object',3,''),
	(729,'2014-03-27 06:45:49',1,13,'671','Diary object',3,''),
	(730,'2014-03-27 06:45:49',1,13,'670','Diary object',3,''),
	(731,'2014-03-27 06:45:49',1,13,'669','Diary object',3,''),
	(732,'2014-03-27 06:45:49',1,13,'668','Diary object',3,''),
	(733,'2014-03-27 06:45:49',1,13,'667','Diary object',3,''),
	(734,'2014-03-27 06:45:49',1,13,'666','Diary object',3,''),
	(735,'2014-03-27 06:45:49',1,13,'665','Diary object',3,''),
	(736,'2014-03-27 06:45:49',1,13,'664','Diary object',3,''),
	(737,'2014-03-27 06:45:49',1,13,'663','Diary object',3,''),
	(738,'2014-03-27 06:45:49',1,13,'662','Diary object',3,''),
	(739,'2014-03-27 06:45:49',1,13,'661','Diary object',3,''),
	(740,'2014-03-27 06:45:49',1,13,'660','Diary object',3,''),
	(741,'2014-03-27 06:45:49',1,13,'659','Diary object',3,''),
	(742,'2014-03-27 06:45:49',1,13,'658','Diary object',3,''),
	(743,'2014-03-27 06:45:49',1,13,'657','Diary object',3,''),
	(744,'2014-03-27 06:45:49',1,13,'656','Diary object',3,''),
	(745,'2014-03-27 06:45:49',1,13,'655','Diary object',3,''),
	(746,'2014-03-27 06:45:49',1,13,'654','Diary object',3,''),
	(747,'2014-03-27 06:45:49',1,13,'653','Diary object',3,''),
	(748,'2014-03-27 06:45:49',1,13,'652','Diary object',3,''),
	(749,'2014-03-27 06:45:49',1,13,'651','Diary object',3,''),
	(750,'2014-03-27 06:45:49',1,13,'650','Diary object',3,''),
	(751,'2014-03-27 06:45:49',1,13,'649','Diary object',3,''),
	(752,'2014-03-27 06:45:49',1,13,'648','Diary object',3,''),
	(753,'2014-03-27 06:45:49',1,13,'647','Diary object',3,''),
	(754,'2014-03-27 06:45:49',1,13,'646','Diary object',3,''),
	(755,'2014-03-27 06:45:49',1,13,'645','Diary object',3,''),
	(756,'2014-03-27 06:45:49',1,13,'644','Diary object',3,''),
	(757,'2014-03-27 06:45:49',1,13,'643','Diary object',3,''),
	(758,'2014-03-27 06:45:49',1,13,'642','Diary object',3,''),
	(759,'2014-03-27 06:45:49',1,13,'641','Diary object',3,''),
	(760,'2014-03-27 06:45:49',1,13,'640','Diary object',3,''),
	(761,'2014-03-27 06:45:49',1,13,'639','Diary object',3,''),
	(762,'2014-03-27 06:45:49',1,13,'638','Diary object',3,''),
	(763,'2014-03-27 06:45:49',1,13,'637','Diary object',3,''),
	(764,'2014-03-27 06:45:49',1,13,'636','Diary object',3,''),
	(765,'2014-03-27 06:45:49',1,13,'635','Diary object',3,''),
	(766,'2014-03-27 06:45:49',1,13,'634','Diary object',3,''),
	(767,'2014-03-27 06:45:49',1,13,'633','Diary object',3,''),
	(768,'2014-03-27 06:45:49',1,13,'632','Diary object',3,''),
	(769,'2014-03-27 06:45:49',1,13,'631','Diary object',3,''),
	(770,'2014-03-27 06:45:49',1,13,'630','Diary object',3,''),
	(771,'2014-03-27 06:45:49',1,13,'629','Diary object',3,''),
	(772,'2014-03-27 06:45:49',1,13,'628','Diary object',3,''),
	(773,'2014-03-27 06:45:49',1,13,'627','Diary object',3,''),
	(774,'2014-03-27 06:45:49',1,13,'626','Diary object',3,''),
	(775,'2014-03-27 06:45:49',1,13,'625','Diary object',3,''),
	(776,'2014-03-27 06:45:49',1,13,'624','Diary object',3,''),
	(777,'2014-03-27 06:45:49',1,13,'623','Diary object',3,''),
	(778,'2014-03-27 06:45:49',1,13,'622','Diary object',3,''),
	(779,'2014-03-27 06:45:49',1,13,'621','Diary object',3,''),
	(780,'2014-03-27 06:45:49',1,13,'620','Diary object',3,''),
	(781,'2014-03-27 06:45:49',1,13,'619','Diary object',3,''),
	(782,'2014-03-27 06:45:49',1,13,'618','Diary object',3,''),
	(783,'2014-03-27 06:45:49',1,13,'617','Diary object',3,''),
	(784,'2014-03-27 06:45:49',1,13,'616','Diary object',3,''),
	(785,'2014-03-27 06:45:49',1,13,'615','Diary object',3,''),
	(786,'2014-03-27 06:45:49',1,13,'614','Diary object',3,''),
	(787,'2014-03-27 06:45:49',1,13,'613','Diary object',3,''),
	(788,'2014-03-27 06:45:49',1,13,'612','Diary object',3,''),
	(789,'2014-03-27 06:45:49',1,13,'611','Diary object',3,''),
	(790,'2014-03-27 06:45:49',1,13,'610','Diary object',3,''),
	(791,'2014-03-27 06:45:49',1,13,'609','Diary object',3,''),
	(792,'2014-03-27 06:45:49',1,13,'608','Diary object',3,''),
	(793,'2014-03-27 06:45:49',1,13,'607','Diary object',3,''),
	(794,'2014-03-27 06:45:49',1,13,'606','Diary object',3,''),
	(795,'2014-03-27 06:45:49',1,13,'605','Diary object',3,''),
	(796,'2014-03-27 06:45:49',1,13,'604','Diary object',3,''),
	(797,'2014-03-27 06:45:49',1,13,'603','Diary object',3,''),
	(798,'2014-03-27 06:45:49',1,13,'602','Diary object',3,''),
	(799,'2014-03-27 06:45:49',1,13,'601','Diary object',3,''),
	(800,'2014-03-27 06:45:49',1,13,'600','Diary object',3,''),
	(801,'2014-03-27 06:45:49',1,13,'599','Diary object',3,''),
	(802,'2014-03-27 06:45:49',1,13,'598','Diary object',3,''),
	(803,'2014-03-27 06:45:49',1,13,'597','Diary object',3,''),
	(804,'2014-03-27 06:45:49',1,13,'596','Diary object',3,''),
	(805,'2014-03-27 06:45:49',1,13,'595','Diary object',3,''),
	(806,'2014-03-27 06:45:49',1,13,'594','Diary object',3,''),
	(807,'2014-03-27 06:45:57',1,13,'593','Diary object',3,''),
	(808,'2014-03-27 06:45:57',1,13,'592','Diary object',3,''),
	(809,'2014-03-27 06:45:57',1,13,'591','Diary object',3,''),
	(810,'2014-03-27 06:45:57',1,13,'590','Diary object',3,''),
	(811,'2014-03-27 06:45:57',1,13,'589','Diary object',3,''),
	(812,'2014-03-27 06:45:57',1,13,'588','Diary object',3,''),
	(813,'2014-03-27 06:45:57',1,13,'587','Diary object',3,''),
	(814,'2014-03-27 06:45:57',1,13,'586','Diary object',3,''),
	(815,'2014-03-27 06:45:57',1,13,'585','Diary object',3,''),
	(816,'2014-03-27 06:45:57',1,13,'584','Diary object',3,''),
	(817,'2014-03-27 06:45:57',1,13,'583','Diary object',3,''),
	(818,'2014-03-27 06:45:57',1,13,'582','Diary object',3,''),
	(819,'2014-03-27 06:45:57',1,13,'581','Diary object',3,''),
	(820,'2014-03-27 06:45:57',1,13,'580','Diary object',3,''),
	(821,'2014-03-27 06:45:57',1,13,'579','Diary object',3,''),
	(822,'2014-03-27 06:45:57',1,13,'578','Diary object',3,''),
	(823,'2014-03-27 06:45:57',1,13,'577','Diary object',3,''),
	(824,'2014-03-27 06:45:57',1,13,'576','Diary object',3,''),
	(825,'2014-03-27 06:45:57',1,13,'575','Diary object',3,''),
	(826,'2014-03-27 06:45:57',1,13,'574','Diary object',3,''),
	(827,'2014-03-27 06:45:57',1,13,'573','Diary object',3,''),
	(828,'2014-03-27 06:45:57',1,13,'572','Diary object',3,''),
	(829,'2014-03-27 06:45:57',1,13,'571','Diary object',3,''),
	(830,'2014-03-27 06:45:57',1,13,'570','Diary object',3,''),
	(831,'2014-03-27 06:45:57',1,13,'569','Diary object',3,''),
	(832,'2014-03-27 06:45:57',1,13,'568','Diary object',3,''),
	(833,'2014-03-27 06:45:57',1,13,'567','Diary object',3,''),
	(834,'2014-03-27 06:45:57',1,13,'566','Diary object',3,''),
	(835,'2014-03-27 06:45:57',1,13,'565','Diary object',3,''),
	(836,'2014-03-27 06:45:57',1,13,'564','Diary object',3,''),
	(837,'2014-03-27 06:45:57',1,13,'563','Diary object',3,'');

/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`)
VALUES
	(1,'log entry','admin','logentry'),
	(2,'permission','auth','permission'),
	(3,'group','auth','group'),
	(4,'user','auth','user'),
	(5,'content type','contenttypes','contenttype'),
	(6,'session','sessions','session'),
	(7,'user social auth','default','usersocialauth'),
	(8,'nonce','default','nonce'),
	(9,'association','default','association'),
	(10,'code','default','code'),
	(11,'migration history','south','migrationhistory'),
	(12,'member','cliders','member'),
	(13,'diary','cliders','diary'),
	(14,'slider','cliders','slider'),
	(15,'slider_photo','cliders','slider_photo'),
	(16,'photo','cliders','photo'),
	(17,'frame','cliders','frame'),
	(18,'diary theme','cliders','diarytheme'),
	(19,'news','cliders','news'),
	(20,'news_photo','cliders','news_photo');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`)
VALUES
	('16zc95b9m5m40a5x93s1y67mpe5cf3it','Mjk3YmJkZjE5YzhmNGRjODJkMjcwYzMwZDcwNzBkM2M0M2JiYWE2NDqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAooBBlUOZmFjZWJvb2tfc3RhdGVYIAAAAFkzcDJkdTlCTktub0hrM0prZEVtUksxVFAzNEVaZUxhVRJfYXV0aF91c2VyX2JhY2tlbmRxA1Unc29jaWFsLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgycQRVBG5leHRVDS9HZXRVc2VySW5mby9xBVUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kcQZYCAAAAGZhY2Vib29rcQdYDwAAAF9zZXNzaW9uX2V4cGlyeXEISv7dTgB1Lg==','2014-05-24 05:30:00'),
	('4yelxn0s01kxtk7hmzime9cnfk3r0bqa','ZDIwMDg4YjJlNTA2ODA0OTY5YjQzMjNiZjJjMzk5YzQ3NGMyNWRlZjqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAooBBlUOZmFjZWJvb2tfc3RhdGVYIAAAAG9qTmVJYnJBZllub2loeW9QUGFXUXhwWUlDYjEyb1owVRJfYXV0aF91c2VyX2JhY2tlbmRxA1Unc29jaWFsLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgycQRVBG5leHRVDS9HZXRVc2VySW5mby9xBVUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kcQZYCAAAAGZhY2Vib29rcQdYDwAAAF9zZXNzaW9uX2V4cGlyeXEISgDkTgB1Lg==','2014-05-19 02:07:25'),
	('68225n1dgx2zwo1gh2grqooo7d2rdb8a','ODU2ZDdhZTY2NzA3NzZjNzljNjEyOTczZGQ4NDI2MjNhMmEwYjkyNzqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABRZ1VDSjNxZG1tb1FaOEVsQlVFS05USG1rZmprYWRNd1UEbmV4dHECVQ0vR2V0VXNlckluZm8vcQN1Lg==','2014-04-02 05:32:31'),
	('7sm16hnmh4b3r7rgsvs03q51yg22d20f','NTg2MzFjODU4OWQxYTkyMGJiNGU1OTBkOTZlMDk2OGU1OTQwZjVhMDqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAAB1cXN6T1M5Q0VUb3BoeGlIcDhwRk5oM04xbHAyWFpmZFUEbmV4dHECVQ0vR2V0VXNlckluZm8vcQN1Lg==','2014-04-17 07:13:23'),
	('frmsme31cmuiu0km6wmzemfdmdym51w0','YzdiYjk5MTQ1N2Q3NGZkZjM4N2U2ODNjMjA5YTM0OWYyNWFkMTk2ZTqAAn1xAShVDV9hdXRoX3VzZXJfaWSKAQZVDmZhY2Vib29rX3N0YXRlWCAAAAAzTVZKVXNIVXRuZlMwRWlWM1RjM2g5aTR1NG5QZGpVSlUSX2F1dGhfdXNlcl9iYWNrZW5kVSdzb2NpYWwuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDJVBG5leHRVDS9HZXRVc2VySW5mby9xAlUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5SgAaTwB1Lg==','2014-06-01 10:50:44'),
	('kxpdrgboszib3phwppet6qhepun5q965','YTkzYzhhNjFkNTAzOWU1ZDM1NTE3ODQyMjBlODkzMWJjMjAwMjgzOTqAAn1xAShVDV9hdXRoX3VzZXJfaWSKAQZVDmZhY2Vib29rX3N0YXRlWCAAAABiQVZ6bzVTY2FUT3Z3VHA4OUM2emZlQ0xkMW9JUXRSZFUSX2F1dGhfdXNlcl9iYWNrZW5kVSdzb2NpYWwuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDJVBG5leHRVDS9HZXRVc2VySW5mby9xAlUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5SqvUTQB1Lg==','2014-05-23 06:21:54'),
	('mdl28l3hmvt7obfft1t38a68ydtt6r0b','ZDkxMGI3YmEzN2VhOTJhN2NlNjE4NGE5ZDFjMzgzMmY2MzU5Yjc0ODqAAn1xAShVDV9hdXRoX3VzZXJfaWSKAQZVDmZhY2Vib29rX3N0YXRlWCAAAAA0UmE1N2N6bzNUOUNkbnJQZHpzTmhGZDQxSXlYRkJHTFUSX2F1dGhfdXNlcl9iYWNrZW5kVSdzb2NpYWwuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDJVBG5leHRVDS9HZXRVc2VySW5mby9xAlUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5SvsZTwB1Lg==','2014-06-02 01:47:06'),
	('uhlcaavskmy52xd4hjg23zilobw0fsh3','ZmU1NGZiNjVmZWZhNzFhYWIwYzE5NjNiNjRmMDFkMWY0MDZjMThjMDqAAn1xAShVDV9hdXRoX3VzZXJfaWSKAQZVDmZhY2Vib29rX3N0YXRlWCAAAABuUndKcEtvTmU1cmJiaUxsOERkMGFFV3pHUTQxV2p5M1USX2F1dGhfdXNlcl9iYWNrZW5kVSdzb2NpYWwuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDJVBG5leHRVDS9HZXRVc2VySW5mby9xAlUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5SgAaTwB1Lg==','2014-06-07 08:15:30');

/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_auth_association
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_auth_association`;

CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table social_auth_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_auth_code`;

CREATE TABLE `social_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(75) NOT NULL,
  `code` varchar(32) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`code`),
  KEY `social_auth_code_09bb5fb3` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table social_auth_nonce
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_auth_nonce`;

CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table social_auth_usersocialauth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_auth_usersocialauth`;

CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(32) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `extra_data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `provider` (`provider`,`uid`),
  KEY `social_auth_usersocialauth_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_e6cbdf29` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;

INSERT INTO `social_auth_usersocialauth` (`id`, `user_id`, `provider`, `uid`, `extra_data`)
VALUES
	(6,5,'facebook','100007902671316','{\"access_token\": \"CAADyLNJEikkBAI6kHfq9HKDOw67UGUVZAdJH0OcZAWl6yOxAp5EeXyYqZAbhTBE5NZAsmLmIdPGzD5yVCvIYLKrWzQjWsV7TAAc6dqOB9RmwYEJUwwRNr72UWT2IyObgwdkRFXcCjBVmk39QDSlzRmszR9kpnGt3WSN4uEks8GZAJFdHfewxU\", \"expires\": \"5121282\", \"id\": \"100007902671316\"}'),
	(7,6,'facebook','1423342436','{\"access_token\": \"CAADyLNJEikkBAJvzuRuJPMnQRRJkURY6esx2yZBr26gRrqGG4sWfIfE2RspeT3kZCHd9wZAuwnCcebZCPvWWCrcvT3qsK9X0C2YqCGhFQZCWp8buOZAg04oPJpqh5foUwdH43y9bc2ExRP6kZCmIjaymkvkia5ZCVeZAYncDJmb61aer2MJJsEgOh\", \"expires\": \"5184000\", \"id\": \"1423342436\"}');

/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table south_migrationhistory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `south_migrationhistory`;

CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;

INSERT INTO `south_migrationhistory` (`id`, `app_name`, `migration`, `applied`)
VALUES
	(1,'cliders','0001_initial','2014-03-14 08:11:24'),
	(2,'cliders','0002_auto__chg_field_member_birthday','2014-03-14 08:29:07'),
	(3,'cliders','0003_auto__add_field_member_head_image','2014-03-14 08:57:53'),
	(4,'cliders','0004_auto__chg_field_member_head_image','2014-03-24 04:15:44'),
	(5,'cliders','0005_auto__chg_field_diary_date','2014-03-25 08:28:26'),
	(6,'cliders','0006_auto__chg_field_slider_date','2014-03-26 06:26:05'),
	(7,'cliders','0007_auto__add_field_slider_describe','2014-03-26 06:34:58'),
	(8,'cliders','0008_auto__add_field_slider_photo_positionLeft__add_field_slider_photo_posi','2014-03-29 08:05:02');

/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
