# -*- coding: utf-8 -*-
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from cliders.models import Member,Diary,Slider,Slider_photo,Photo,Frame,DiaryTheme,News,News_photo # import database
from django.http import HttpResponse
from django.utils.http import urlquote,urlunquote
from django.contrib import auth
from django.contrib.auth import logout
from PIL import Image
import mimetypes 
import imghdr
from django.core.files import File
import json,requests,uuid,datetime,os,cStringIO,re


# from social.apps.django_app.me.models import UserSocialAuth 
# Create your views here.
def index(request):
	if request.user:
		logout(request)
	# print(request.user)
	return render(request,'index.html')

def LoginError(request):
	HttpResponse("what the ERROR!!")

def Logout(request):
	logout(request)
	return redirect('/')

# check the user authenticated and get user infomation
def GetUserInfo(request):
	user_dict = {}
	if request.user.is_authenticated():
		user_dict.update({'user':request.user})
		social_auth = request.user.social_auth.get(provider='facebook')
		#get info from facebook
		fb_params = {'access_token': social_auth.extra_data['access_token']}
		fb_response = requests.get('https://graph.facebook.com/me', params=fb_params)
		fb_picture = requests.get('https://graph.facebook.com/me/picture?redirect=0&height=200&type=normal&width=200', params=fb_params)
		user_dict.update({'fb_user': fb_response.json(),'fb_pro_pic':fb_picture.json()})
		user_fb = user_dict['fb_user']
		try:
			# if user exist update user info
			user = Member.objects.get(user=user_dict['user'])
			user.name = user_fb['name']
			user.sex = user_fb['gender']
			user.email = user_fb['email']
			fb_birthday = user_fb['birthday'].split('/')
			user.birthday = fb_birthday[2]+'-'+fb_birthday[0]+'-'+fb_birthday[1]
			user.head_image = user_dict['fb_pro_pic']['data']['url']
			user.save()
		except Member.DoesNotExist:
			#else if user is not exist add user
			fb_head_url = user_dict['fb_pro_pic']['data']['url']
			print(user_dict['fb_pro_pic']['data']['url'])
			fb_birthday = user_fb['birthday'].split('/')
			new_user_birthday = fb_birthday[2]+'-'+fb_birthday[0]+'-'+fb_birthday[1]
			new_user = Member(user=request.user,name=user_fb['name'],sex=user_fb['gender'],birthday=new_user_birthday,email=user_fb['email'],head_image=user_dict['fb_pro_pic']['data']['url'])
			new_user.save()
		return redirect('/Home/')
	else:
		return render(request,'index.html')

# personal home page
def Home(request,yymm=None):
	if request.user.is_authenticated():
		if yymm==None:
			YY=datetime.date.today().strftime("%Y")
			MM=datetime.date.today().strftime("%m")
			print(YY,MM)
		else:
			YY=yymm.split('-')[0]
			MM=yymm.split('-')[1]
			print(YY,MM)
		today = datetime.date.today().strftime("%y-%m-%d")
		yymmdd = str(today).split('-')
		date = yymmdd[1]+'.'+yymmdd[2]
		weekday = ChineseWeekday(datetime.date.today().strftime("%A"))
		user_m = Member.objects.get(user=request.user)
		festival = FestivalCheck(date)
		user_diary = GetUserDiary(request.user,YY,MM)
		# print(user_diary)
		return render ( request,'home.htm',{'user_name':user_m.name,'head':user_m.head_image,'date':date,'weekday':weekday,'festival':festival,'user_diary':user_diary,'YY':YY,'MM':MM})
	else:
		return redirect('/')

# change the weekday to chinese
def ChineseWeekday(weekday):
	if weekday == 'Monday':
		return '星期一'
	elif weekday == 'Tuesday':
		return '星期二'
	elif weekday == 'Wednesday':
		return '星期三'
	elif weekday == 'Thursday':
		return '星期四'
	elif weekday == 'Friday':
		return '星期五'
	elif weekday == 'Saturday':
		return '星期六'
	elif weekday == 'Sunday':
		return '星期日'
	else:
		return ''

# change if today is th specialday
def FestivalCheck(today):
	if today == '04.09':
		return '今天是Macbook pro 跳水節'
	elif today == '02.14':
		return '情人節'
	else:
		return '平日'
# chagne the weekday to chinese abbr.
def ChineseWeekDayShort(weekday):
	day = ''
	if weekday == 'Sun':
		day = '(日)'
	elif weekday == 'Mon':
		day = '(ㄧ)'
	elif weekday == 'Tue':
		day = '(二)'
	elif weekday == 'Wed':
		day = '(三)'
	elif weekday == 'Thu':
		day = '(四)'
	elif weekday == 'Fri':
		day = '(五)'
	elif weekday == 'Sat':
		day = '(六)'
	else:
		day = ''
	day = day.decode('utf-8')
	return day

# get all diary content of specific user
def GetUserDiary(user,yy,mm):
	member = Member.objects.get(user=user)
	user_diary = ''
	date_temp = ''
	all_diary = Diary.objects.filter(member=member,date__year=yy,date__month=mm).order_by('-date')
	for i,diary in enumerate(all_diary):
		diary_dict = {}
		diary_photos = Photo.objects.filter(diary=diary)
		diary_dict.update({'yymmdd':diary.date.isoformat(),'date':diary.date.strftime("%d") + '<br><p>' + ChineseWeekDayShort(diary.date.strftime("%a")).encode('utf-8')+'</p>','id':diary.diary_id.encode('utf-8')})
		if diary.title == None:
			diary_dict.update({ 'title': ' ' })
		else:
			diary_dict.update({ 'title': diary.title.encode('utf-8') })

		if diary.shared == True:
			diary_dict.update({ 'shared':'/static/css/icon/public.png' })
		else:
			diary_dict.update({ 'shared':'/static/css/icon/fb_share.png' })
		photos = ''
		for photo in diary_photos:
			photos = photos + str('<img src="/media/'+urlunquote(photo.resize_image).decode('utf-8') + '" />').encode('utf-8')
		diary_dict.update({'photos':photos})
		if(i==len(all_diary)-1):
			if(date_temp != diary_dict['yymmdd']):
				user_diary += '<div  class="row"> \
								<div class="col-xs-2"> \
									<div class="line"></div> \
										<div data-date="' + diary_dict['yymmdd'] + '" class="diary_date">'+diary_dict['date']+'</div> \
								</div>'
			else:
				user_diary += '<div  class="row"> \
								<div class="col-xs-2"> \
									<div class="line line_repeat"></div> \
										<div data-date="' + diary_dict['yymmdd'] + '" class="diary_date diary_date_repeat"></div> \
								</div>'			
		else:
			if(date_temp != diary_dict['yymmdd']):
				user_diary += '<div  class="row"> \
								<div class="col-xs-2"> \
									<div class="line"></div> \
										<div data-date="' + diary_dict['yymmdd'] + '" class="diary_date">'+diary_dict['date']+'</div> \
									<div class="line"></div> \
								</div>'
			else:
				user_diary += '<div  class="row"> \
								<div class="col-xs-2"> \
									<div class="line line_repeat"></div> \
										<div data-date="' + diary_dict['yymmdd'] + '" class="diary_date diary_date_repeat"></div> \
									<div class="line line_repeat"></div> \
								</div>'
		date_temp = diary_dict['yymmdd']
		user_diary += '<div class="col-xs-10 diary_panel" > \
						<div class="diary_content"> \
							<div data-id="'+diary_dict['id']+'" class="diary_title">'+diary_dict['title']+'</div> \
							<div class="diary_func_div"> \
								<img data-id="'+diary_dict['id']+'" data-n="'+str(i)+'" class="diary_share" src="' + diary_dict['shared'] + '"/> \
								<img data-id="'+diary_dict['id']+'" data-n="'+str(i)+'" class="diary_delete" src="/static/css/icon/delete.png"/> \
								<img data-id="'+diary_dict['id']+'" data-n="'+str(i)+'" class="diary_edit" src="/static/css/icon/modify.png"/> \
							</div> \
							<div class="diary_photo"><div><div class="photo_drag">'+diary_dict['photos']+'</div></div></div> \
						</div> \
					</div> \
				</div>'
	return user_diary

def FacebookShare(request):
	if request.user.is_authenticated():
		user_dict={}
		user_dict.update({'user':request.user})
		social_auth = request.user.social_auth.get(provider='facebook')
		pageid = request.POST.get('pageid')
		diary = Diary.objects.get(diary_id=pageid)
		if diary.shared:
			diary.shared = False;
			diary.save()
			return HttpResponse(request,{'success':'success'})
		else:
			diary.shared = True
			diary.save()
			print(diary.shared)
			fb_params = {'access_token': social_auth.extra_data['access_token']}
			user_dict.update({ 'slogon' : '!'})
			user_dict.update({ 'link' : 'clide.rs:8001/Diary/'+ pageid})
			requests.post('https://graph.facebook.com/'+social_auth.extra_data['id']+'/feed?message='+user_dict['slogon']+'&link='+user_dict['link'], params=fb_params)
			return HttpResponse(request,{'success':'success'})
	else:
		return redirect('/')

def PresentDiary(request,pageid=None):
	if pageid==None:
		return redirect('/')
	else:
		diary = Diary.objects.get(diary_id=pageid)
		return render(request,'mydiary.htm',{ 'diary_content':diary.diary_content,'diary_title':diary.title,'diary_date':diary.date })


# add a new diary that save with a new diary id and return diary id
def AddDiary(request,pageid=None):
	user_dict={}
	if request.user.is_authenticated():
		# if pageid = None, then add a new diary
		if pageid is None:
			text_editor = EditorTemplate('none','')
			return render(request,'DiarySetting.htm',{'editor_template':text_editor})
		else:
			if Diary.objects.filter(diary_id=pageid).count()!=1:
				# something wrong
				return redirect('/')
			else:
				diary = Diary.objects.get(diary_id=pageid)
				text_editor = EditorTemplate(pageid,diary.diary_content)
				return render(request,'DiarySetting.htm',{'editor_template':text_editor,'diary_content':diary.diary_content,'title':diary.title})


	# if user don't have the right to load the data than return to index
	else:
		return redirect('/')

def DeleteDiary(request):
	if request.user.is_authenticated():
		pageid = request.POST.get('pageid')
		Diary.objects.get(diary_id=pageid).delete()
		return HttpResponse(request,{'success':'success'})
	else:
		return HttpResponse(request,{'error':'你沒有權限刪除此文章！'})

# upload data-uri(html5) image and save image 
def DiaryImgSave(request):
	if request.user.is_authenticated():
		import base64
		diary_id = request.POST.get('diary_id').encode('utf-8')
		diary = Diary.objects.get(diary_id=diary_id)
		dataUrl = request.POST.get('dataUrl')
		diary_photo_J = []
		# convert to image and save image
		img_id = uuid.uuid4().hex
		print(img_id)
		img_type = str((mimetypes.guess_type(dataUrl))[0]).split('/')[1]
		imgstr = re.search(r'base64,(.*)',dataUrl).group(1)
		tempimg = imgstr.decode('base64')
		f = open('%s.%s' % (img_id, img_type), 'wb')
		f.write(tempimg)
		f.close()
		#image resize 
		max_img_h = 800
		im = Image.open(img_id+'.'+img_type,'r')
		im_w,im_h = im.size
		if (im_w*im_h) > 5000000:
			return HttpResponse(json.dumps({"error":"照片大小不得超過5MB!!"}))
		
		if(im_h > max_img_h ):
			max_img_w = im_w*max_img_h/im_h
			im_resize = im.resize((max_img_w,max_img_h),Image.ANTIALIAS)
			im_resize.save('media/image/'+img_id+'.'+img_type,format=img_type)
			print(im_resize)
			photo = Photo(diary=diary,origin_image=File(open(img_id+'.'+img_type,'r')),resize_image=File(open('media/image/'+img_id+'.'+img_type,'r')))
			os.remove('media/image/'+img_id+'.'+img_type)
			photo.save()
		else:
			
			photo = Photo(diary=diary,origin_image=File(open(img_id+'.'+img_type,'r')),resize_image=File(open(img_id+'.'+img_type,'r')))
			photo.save()

		os.remove(img_id+'.'+img_type)
		resize_img = "/"+urlunquote(photo.resize_image.url)
		# return resize image url
		return HttpResponse(json.dumps({'resize_img': resize_img}))
	else:
		return redirect('/')

#save user diary content 
def DiarySave(request):
	if request.user.is_authenticated():
		auth_user = request.user
		if Member.objects.filter(user=auth_user).count()==1:
			# diary save function 
			title = request.POST.get('title')
			if len(title)>10:
				return HttpResponse(json.dumps({"error" : "日記標題不能超過10個字！"}))
			if len(title)==0:
				title = '無'
			diary_id = request.POST.get('diary_id')
			if diary_id == '':
				# ================
				# save a new diary
				# ================
				diary = request.POST.get('diary')
				if ImageOverSizeJudge(diary):
					owner = Member.objects.get(user=auth_user)
					while True:
						diary_id = uuid.uuid4().hex
						if Diary.objects.filter(diary_id=diary_id).count()==0:
							break
					new_diary = Diary(member=owner,title=title,diary_id=diary_id,date=datetime.date.today(),shared=False)
					new_diary.save()
					for r in diary.split('"'):
						diary = SaveImage(diary,r,new_diary)
					# print('------------------------')
					# print(diary)
					# print('------------------------')
					if len(diary) > 4800:
						return HttpResponse(json.dumps({"error":"內容字數過多!"}))

					new_diary.diary_content = diary
					new_diary.save()
				else:
					return HttpResponse(json.dumps({"error" : "照片大小不能超過4MB!"}))
			else:
				# =====================
				# save a existing diary
				# =====================
				if Diary.objects.filter(member=Member.objects.get(user=auth_user),diary_id=diary_id).count() !=1:
					# something wrong 
					return redirect('/')
				else:
					exist_diary = Diary.objects.get(member=Member.objects.get(user=auth_user),diary_id=diary_id)
					diary = request.POST.get('diary')
					if ImageOverSizeJudge(diary):						
						for r in diary.split('"'):
							diary = SaveImage(diary,r,exist_diary)
						# print('------------------------')
						# print(diary)
						# print('------------------------')
						if len(diary) > 4800:
							return HttpResponse(json.dumps({"error":"內容字數過多!"}))

						exist_diary.diary_content = diary
						exist_diary.title = title
						exist_diary.save()
					else:
						return HttpResponse(json.dumps({"error" : "照片大小不能超過4MB!"}))
			return HttpResponse(json.dumps({"success":"diary saved."}))
		else:
			return HttpResponse(json.dumps({"error" : "抱歉，你沒有權限瀏覽這個網頁!!"}))
	else:
		return redirect('/')
# judge if content image is oversize
def ImageOverSizeJudge(content):
	for k in content.split('"'):
		if 'data:image/' in k:
			rest = k.split('data:image/')[1]
			# print(rest)
			s = rest.split(';base64,')
			img_type = s[0]
			# get img content string
			imgstr = s[1]
			# get unqiue img uid
			img_id = 'media/image_processing/' + uuid.uuid4().hex
			while True:
				img_id = 'media/image_processing/' + uuid.uuid4().hex
				if os.path.isfile(img_id+'.'+img_type) != True:
					break
			print(img_id,img_type)
			tempimg = imgstr.decode('base64')
			f = open('%s.%s' % (img_id, img_type), 'wb')
			f.write(tempimg)
			f_size = f.tell()
			f.close()
			if f_size > 4000000 :
				os.remove(img_id+'.'+img_type)
				return False
			os.remove(img_id+'.'+img_type)
	return True
# save diary photo and replace the url 
def SaveImage(content,r,target):
	rest = r.split('data:image/')
	for k in rest:
		# get individual file name
		if ';base64,' in k:
			s = k.split(';base64,')
			img_type = s[0]
			# get img content string
			imgstr = s[1].split('"')[0]

			# get unqiue img uid
			img_id = 'media/image_processing/' + uuid.uuid4().hex
			while True:
				img_id = 'media/image_processing/' + uuid.uuid4().hex
				if os.path.isfile(img_id+'.'+img_type) != True:
					break
			tempimg = imgstr.decode('base64')
			f = open('%s.%s' % (img_id, img_type), 'wb')
			f.write(tempimg)
			# f_size = f.tell()
			f.close()
			 
			im = Image.open(img_id+'.'+img_type)
			im_w,im_h = im.size
			if ( im_h > 800):
				new_h = 800
				new_w = im_w*800/im_h
				im_resize = im.resize((new_w, new_h), Image.ANTIALIAS)
				resize_id = 'media/image_processing/resize/' + uuid.uuid4().hex
				im_resize.save( resize_id + '.' + img_type, format=img_type)
				new_img = Photo(origin_image=File(open(img_id+'.'+img_type,'r')),resize_image=File(open(resize_id+'.'+img_type,'r')),diary=target)	
				os.remove(resize_id+'.'+img_type)
			else:
				new_img = Photo(origin_image=File(open(img_id+'.'+img_type,'r')),resize_image=File(open(img_id+'.'+img_type,'r')),diary=target)	
			new_img.save()
			content = content.replace(r,"/"+urlunquote(new_img.resize_image.url))
			os.remove(img_id+'.'+img_type)
	return content
# judge if the image exist or not 
def NewsImgRearrange(content,target,org):
	news_img_all = NewsImage.objects.filter(news=target)
	for k in content.split('"'):
		if org.englishName in k:
			for img in news_img_all:
				print(' folder name:' + org.englishName)
				print('k:' + k)
				print('url: '+"/"+urlunquote(img.image.url) )
				if k != "/"+urlunquote(img.image.url):
					# print(str(img.id)+' is delete')
					img.delete()
				else:
					# print('pass')
					pass

# get user all diary 
def UserDiary(request):
	if request.user.is_authenticated():
		auth_user = Member.objects.filter(user=request.user)
		if Member.objects.filter(user=auth_user).count()==1:
			all_diary = []
			today = datetime.date.today()
			user_diary = Diary.objects.filter(member=auth_user,date__year=today.year,date__month=today.month)
			for diary in user_diary:
				date = diary.date.strftime("%y-%m-%d")
				all_diary = all_diary + [{'title':diary.title,'content':diary.diary_content,'date':date,'id':diary.diary_id }]
			user_diary = json.dumps(all_diary)
			return render(request,'user_diary.htm',{'user_diary':user_diary})
		else:
			return redirect('/')
	else:
		return redirect('/')

def AddSlide(request):
	if request.user.is_authenticated():
		auth_user = Member.objects.filter(user=request.user)
		if auth_user.count()==1:
			today = datetime.date.today()
			user_diary = Diary.objects.filter(member=auth_user,date__year=today.year,date__month=today.month)
			photos = ''
			for diary in user_diary:
				diary_photo = Photo.objects.select_related('diary').filter(diary=diary).values('resize_image')
				for photo in diary_photo:
					photos += AllPhotosTemplate(photo)
			return render(request,'SlideSetting.htm',{'photos':photos,'date':today})
		else:
			return HttpResponse("What a wonderful world!!")
	else:
		return HttpResponse("What a wonderful world!!")

def SaveSlideOrder(request):
	if request.user.is_authenticated():
		auth_user = Member.objects.filter(user=request.user)
		if auth_user.count()==1:
			slider_id = request.POST.get('id');
			slider_obj = Slider(slider_id=slider_id,member=Member.objects.get(user=request.user),date=datetime.date.today())
			slider_obj.save()
			slider_photos = getDictArray(request.POST,'slider_photos')
			# print(slider_photos[0]['url'])
			for i,value in slider_photos.iteritems():
				photo = Slider_photo(slider=slider_obj,image_url=value['url'],order=value['order'])
				photo.save()
		return HttpResponse("What a wonderful world!!") 
	else:
		return HttpResponse("What a wonderful world!!")

# function to handle multi-dimension array
def getDictArray(post, name):
	dic = {}
	for k in post.keys():
		rest = k[len(name):]
		# print(rest)
		# split the string into different components
		parts = [p[:-1] for p in rest.split('[')][1:]
		# print(parts)
		# print(len(parts))
		if len(parts)!=0:
			id = int(parts[0])
			# add a new dictionary if it doesn't exist yet
			if id not in dic:
				dic[id] = {}
			# add the information to the dictionary
			dic[id][parts[1]] = post.get(k)
	return dic

#========================================================================================
#=================================== assembly template function =========================
#========================================================================================
def EditorTemplate(diary_id,content):
	editor_template = '<div id="editor_toolbar" class="btn-toolbar" data-role="editor-toolbar" data-target="#editor_content"> \
			<div class="btn-group"> \
				<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a> \
				<ul class="dropdown-menu"> \
					<li><a data-edit="fontSize 5"><font size="5">大</font></a></li> \
					<li><a data-edit="fontSize 3"><font size="3">中</font></a></li> \
					<li><a data-edit="fontSize 1"><font size="1">小</font></a></li> \
				</ul> \
			</div> \
			<div class="btn-group"> \
				<a class="btn btn-md btn-info" data-edit="bold" title data-original-title="Bold"><i class="fa fa-bold"></i></a> \
				<a class="btn btn-md btn-info" data-edit="italic" title data-original-title="Italic"><i class="fa fa-italic"></i></a> \
				<a class="btn btn-md btn-info" data-edit="strikethrough" title data-original-title="Strikethrough"><i class="fa fa-strikethrough"></i></a> \
				<a class="btn btn-md btn-info" data-edit="underline" title data-original-title="Underline"><i class="fa fa-underline"></i></a> \
			</div> \
			<div class="btn-group"> \
				<a class="btn btn-md btn-info" data-edit="insertunorderedlist" title data-original-title="Bullet List"><i class="fa fa-list-ul"></i></a> \
				<a class="btn btn-md btn-info" data-edit="insertorderedlist" title data-original-title="Number List"><i class="fa fa-list-ol"></i></a> \
				<a class="btn btn-md btn-info" data-edit="outdent" title data-original-title="Reduce indent"><i class="fa fa-indent"></i></a> \
				<a class="btn btn-md btn-info" data-edit="indent" title data-original-title="Indent"><i class="fa fa-dedent"></i></a> \
			</div> \
			<div class="btn-group"> \
				<a class="btn btn-md btn-info" data-edit="justifyleft" title data-original-title="Align Left"><i class="fa fa-align-left"></i></a> \
				<a class="btn btn-md btn-info" data-edit="justifycenter" title data-original-title="Center"><i class="fa fa-align-center"></i></a> \
				<a class="btn btn-md btn-info" data-edit="justifyright" title data-original-title="Align Right"><i class="fa fa-align-right"></i></a> \
				<a class="btn btn-md btn-info" data-edit="justifyfull" title data-original-title="Justify"><i class="fa fa-align-justify"></i></a> \
			</div> \
			<div class="btn-group"> \
				<a id="img_upload" class="btn" data-original-title="Insert picture"><i class="glyphicon glyphicon-picture"></i></a> \
				<input id="add_diary_photo" type="file" data-role="magic-overlay"  data-target="#img_upload" data-edit="insertImage" /> \
			</div> \
			<div class="btn-group"> \
				<a class="btn btn-md btn-info" data-edit="undo" title data-original-title="Undo"><i class="fa fa-undo"></i></a> \
				<a class="btn btn-md btn-info" data-edit="redo" title data-original-title="Redo"><i class="fa fa-repeat"></i></a> \
			</div> \
		</div>\
		<div id="editor_content" data-id="'+str(diary_id)+'">'+ str(content) +'</div>'
	return editor_template
def AllPhotosTemplate(photos):
	photo = ''
	photo = '<div class="imgDiv"><input class="hide" type=checkbox />\
				<div class="check_box"><img src="/static/css/icon/checkbox.png"></div><img src="'+"/media/"+photos['resize_image']+'"></div>'
	return photo
