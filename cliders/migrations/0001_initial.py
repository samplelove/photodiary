# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Member'
        db.create_table(u'cliders_member', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100)),
            ('sex', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('birthday', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'cliders', ['Member'])

        # Adding model 'Diary'
        db.create_table(u'cliders_diary', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Member'])),
            ('diary_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('diary_content', self.gf('django.db.models.fields.CharField')(max_length=5000)),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
        ))
        db.send_create_signal(u'cliders', ['Diary'])

        # Adding model 'Slider'
        db.create_table(u'cliders_slider', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Member'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('slider_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'cliders', ['Slider'])

        # Adding model 'Slider_photo'
        db.create_table(u'cliders_slider_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slider', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Slider'])),
            ('image_url', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('describe', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('order', self.gf('django.db.models.fields.IntegerField')(max_length=15)),
        ))
        db.send_create_signal(u'cliders', ['Slider_photo'])

        # Adding model 'Photo'
        db.create_table(u'cliders_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('diary', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Diary'])),
            ('origin_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('resize_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('positionLeft', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('positionRight', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('zoom', self.gf('django.db.models.fields.FloatField')(default=1)),
        ))
        db.send_create_signal(u'cliders', ['Photo'])

        # Adding model 'Frame'
        db.create_table(u'cliders_frame', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('diary', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Diary'])),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('background_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('theme', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('background_color', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'cliders', ['Frame'])

        # Adding model 'DiaryTheme'
        db.create_table(u'cliders_diarytheme', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('diary', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.Diary'])),
            ('theme', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('background_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('icon', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('button', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'cliders', ['DiaryTheme'])

        # Adding model 'News'
        db.create_table(u'cliders_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 3, 14, 0, 0), null=True)),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=2500, null=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('image_url', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'cliders', ['News'])

        # Adding model 'News_photo'
        db.create_table(u'cliders_news_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('news', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliders.News'])),
            ('image_url', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'cliders', ['News_photo'])


    def backwards(self, orm):
        # Deleting model 'Member'
        db.delete_table(u'cliders_member')

        # Deleting model 'Diary'
        db.delete_table(u'cliders_diary')

        # Deleting model 'Slider'
        db.delete_table(u'cliders_slider')

        # Deleting model 'Slider_photo'
        db.delete_table(u'cliders_slider_photo')

        # Deleting model 'Photo'
        db.delete_table(u'cliders_photo')

        # Deleting model 'Frame'
        db.delete_table(u'cliders_frame')

        # Deleting model 'DiaryTheme'
        db.delete_table(u'cliders_diarytheme')

        # Deleting model 'News'
        db.delete_table(u'cliders_news')

        # Deleting model 'News_photo'
        db.delete_table(u'cliders_news_photo')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'cliders.diary': {
            'Meta': {'object_name': 'Diary'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'diary_content': ('django.db.models.fields.CharField', [], {'max_length': '5000'}),
            'diary_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Member']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        u'cliders.diarytheme': {
            'Meta': {'object_name': 'DiaryTheme'},
            'background_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'button': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'diary': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Diary']"}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'theme': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'cliders.frame': {
            'Meta': {'object_name': 'Frame'},
            'background_color': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'background_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'diary': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Diary']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'theme': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'})
        },
        u'cliders.member': {
            'Meta': {'object_name': 'Member'},
            'birthday': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'cliders.news': {
            'Meta': {'object_name': 'News'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '2500', 'null': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 14, 0, 0)', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'cliders.news_photo': {
            'Meta': {'object_name': 'News_photo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.News']"})
        },
        u'cliders.photo': {
            'Meta': {'object_name': 'Photo'},
            'diary': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Diary']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'origin_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'positionLeft': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'positionRight': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'resize_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'zoom': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        u'cliders.slider': {
            'Meta': {'object_name': 'Slider'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Member']"}),
            'slider_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '75'})
        },
        u'cliders.slider_photo': {
            'Meta': {'object_name': 'Slider_photo'},
            'describe': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {'max_length': '15'}),
            'slider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliders.Slider']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['cliders']