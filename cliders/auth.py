# -*- coding: utf-8 -*-
from cliders.models import *
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.utils.http import urlquote,urlunquote
from django.contrib import auth
from django.contrib.auth import logout
from PIL import Image
import mimetypes 
import imghdr
from django.core.files import File
import json,requests,uuid,datetime,os,cStringIO,re


def create_profile(user, is_new=False, *args, **kwargs):
    if is_new:
        # create a profile instance for the given user
        create_user_profile(user)