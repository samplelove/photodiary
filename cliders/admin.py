from django.contrib import admin
from django.db import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from cliders.models import Member,Diary,Slider,Slider_photo,Photo,Frame,DiaryTheme,News,News_photo

class MemberAdmin(admin.ModelAdmin):
	list_display = ['email','sex','birthday','name']

class DiaryAdmin(admin.ModelAdmin):
	list_display = ['member','diary_id','diary_content','date','title']

class SliderAdmin(admin.ModelAdmin):
	list_display = ['member','slider_id','title','date']

class Slider_photoAdmin(admin.ModelAdmin):
	list_display = ['slider','image_url','describe','order']

class PhotoAdmin(admin.ModelAdmin):
	list_display = ['diary','origin_image','resize_image','positionLeft','positionRight','zoom']

class FrameAdmin(admin.ModelAdmin):
	list_display = ['diary','color','background_image','theme','background_color']

class DiaryThemeAdmin(admin.ModelAdmin):
	list_display = ['diary','theme','background_image','icon','logo','button']

class NewsAdmin(admin.ModelAdmin):
	list_display = ['date','content','title','image_url']

class News_photoAdmin(admin.ModelAdmin):
	list_display = ['news','image_url']

# Register your models here.

admin.site.register(Member,MemberAdmin)
admin.site.register(Diary,DiaryAdmin)
admin.site.register(Slider,SliderAdmin)
admin.site.register(Slider_photo,Slider_photoAdmin)
admin.site.register(Photo,PhotoAdmin)
admin.site.register(Frame,FrameAdmin)
admin.site.register(DiaryTheme,DiaryThemeAdmin)
admin.site.register(News,NewsAdmin)
admin.site.register(News_photo,News_photoAdmin)