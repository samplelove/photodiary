// query objects
var $diary_image_upload = $("#add_diary_photo");
var $diary_content = $("#diary_content");
var $img_upload =$("#img_upload");
var $update_diary = $("#update_diary");
var $save_diary = $("#save_diary");
var $diary_title = $("#diary_title");
window.diary_photo = new Array();

//text editor initial setting
$(window).load(function(){
	$diary_content.height($(window).height()-$('#diary_title').height()-30);
	//enable div editor
	$("#editor_content")
		.css('min-height',$diary_content.height()-$("#editor_toolbar").height()-$save_diary.height()-35)
		.wysiwyg();

	$diary_content.focus();

	//align the element
	$save_diary.click(function(e){
		e.preventDefault();
		var content = $("#editor_content").html();
		var id = $("#editor_content").attr('data-id');
		var title = $('#title').val();
		if(title.length>10)
			alert('日記標題不能超過10個字！');
		else 
			SaveContent(id,content,title);
	});

});
function SaveContent(id,text,title){
	console.log('id: '+id);
	if(id=="none")
		var diary_id ='';
	else 
		var diary_id = id;
	$.ajax({
		type:'POST',
		datatype:'JSON',
		data:{'diary_id':diary_id,'diary':text,'title':title},
		url:'/DiarySave/',
		success: function(Jdata){
			if((JSON.parse(Jdata)).error){
				alert((JSON.parse(Jdata)).error);
			}else if((JSON.parse(Jdata)).success){
				window.location.href = '/Home/'
			}
		},
		error :function(Jdata){
			alert('網路連線錯誤，請聯絡管理員！');
		}
	})
}
//replace special character for bootstrap-wysiwyg text editor
function ReplaceInputStr(uStr) {
	return uStr.replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;").replace(/\\/g,"&nbsp");
}

function ReplaceInputStrToText(uStr) {
	return uStr.replace(/<br>/g,"\n").replace(/&nbsp/g," ").replace(/&#34;/g,'"').replace(/&#39;/g,"'");
}

// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});