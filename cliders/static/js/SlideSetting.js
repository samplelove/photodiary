var $photo_tank = $("#photo_tank");
var $selected_photo = $("#selected_photo");
var $resize_container = $("#resize_container");

$(window).load(function(){
	AlignItems();
	// window.location.hash = 'album';
	// setting the width and the hieght of the resize container
	// $resize_container
	// 	.height($(window).height())
	// 	.width($(window).width())
	// 	.css('margin-left',$(window).width());
	// put all img into the container
	// ImgAppend(function(){
	// 	SortableDiv();
	// });
	//tab switch div visible function
	// TabClick(function(){
	// 	ImgRealign();
	// });
	//while checkbox is true the append the selected photo into target div
	// var $input = $("input",$photo_tank);
	// $input.change(function(){
	// 	if($(this).is(":checked")){
	// 		var photo = $(this).parent().clone();
	// 		$(photo).appendTo($selected_photo).prepend('<p class="order_mask"></p>').attr('data-toggle','modal').attr('data-target','#photo_adjust');
	// 		$('input',photo).remove();
	// 		OrderMaskSizeAdjust();
	// 	}else if($(this).is(":not(:checked)")){
	// 		//find the photo in the selected_photo and remove it
	// 		var div_id =  $(this).parent().attr('id');
	// 		$("div",$selected_photo).each(function(i,el){
	// 			if($(el).attr('id')==div_id){
	// 				$(el).remove();
	// 			}else;
	// 		});
	// 	}else;
	// });
	//click to save picture order
	$("#save_slide").click(function(e){
		e.preventDefault();
		SaveSlideOrder();
	});

	// trigger edit pictrun function
});
function AlignItems(){
	// reszie or align function
	$('div','#slide_toolbar').each(function(i,el){
		var $img = $('img',el);
		var l = $(el).width();
		$(el).height(l).css({'margin-top':-l*0.63});
		$img.css({
			'margin-left':(l-$img.width())/2+3,
			'margin-top':(l-$img.height())/2+3,
		});		
	});
	// realign img of all album 
	$('img','#photo_tank').each(function(i,el){
		// console.log('hi hi.');
		// console.log('width: '+el.width);
		// console.log('height: '+el.height);
		if(el.width>el.height){
			$(el).addClass('widthMax').css({
				'margin-top':($(el).parent().height()-el.height)/2,
			});
		}else if(el.width<el.height){
			$(el).addClass('heightMax').css({
				'margin-left':($(el).parent().width()-el.width)/2,
			});
		}else{
			$(el).addClass('square').css({
				'margin-top':($(el).parent().height()-el.height)/2,
				'margin-left':($(el).parent().width()-el.width)/2,
			});;
		}
	});
	// calculate the height of photo tank
	var $imgDiv = $('.imgDiv');
	$photo_tank.height(Math.ceil($imgDiv.length/2)*($imgDiv.height()+5)+10);

	$imgDiv.each(function(i,el){
		$(el).click(function(){
			$('img',this).css('opacity',1);
			$('intput[type=checkbox]',this).attr();
			console.log($('input[type=checkbox]:checked','.imgDiv').length+'is checked.');
		});
	});



}

// font mask on the selected_photo img div
function OrderMaskSizeAdjust(){
	var height = $("div",$photo_tank).height();
	var width = $("div",$photo_tank).width();
	$(".order_mask").css({
		"height":height,
		"width":width,
	});
}

//fix the layout when window is resizing
$(window).resize(function(){
	ImgRealign();
});

//call jQuery sortable init function 
function SortableDiv(){
	$selected_photo.sortable({
		revert:300,
		containment:"parent",
		opacity:0.65,
		cursor:"move",
		dropOnEmpty:false,
		tolerance:"pointer",
		forcePlaceholderSize:true,
		placeholder:"col-xs-12 col-sm-6 col-md-4 col-lg-4 well sort_example imgDiv",
		start: function(event,ui){
			var $order = $(".order_mask");
			$order.css("opacity",0.8);
			// console.log($order.length);
			$order.each(function(i,el){
				$(el).html((i+1));
			});
		},
		stop: function(event,ui){ // update slider photo order
			var $order = $(".order_mask");
			if($order.length){
				// rearrange img order and url
				GetSortArray();
			}else{
				alert("Please select photos to make a slider.");
			}
			$order.css("opacity","");
		},
	});
}
// get the array of the sorted-img
function GetSortArray(){
	var $img = $("img",$photo_tank);
	window.slider_img = new Array();
	var $order = $(".order_mask");
	window.$order_obj = $selected_photo.sortable("toArray");

	for(var i=0;i<$order.length;i++){
		var order = parseInt($order_obj[i]).toString();
		var num = order.slice(6);
		var url = $img[num].src;
		var left = $img[num].left;
		var top = $img[num].top;
		var describe = $img[num].describe;
		var zoom = $img[num].zoom;
		window.slider_img.push({"order":i,"url":url,"left":left,"top":top,"describe":describe,"zoom":zoom});
	}
}
//click tab  to switch content page
function TabClick(callback){
	var $li = $("li",".nav");
	$li.click(function(){
		$li.each(function(index,el){
			$(el).removeClass('active');
		});
		var $this = $(this);
		$this.addClass('active');
		// switch the content div
		if($this.attr('data-id')=='photo_tank'){
			window.location.hash = 'album';
			$selected_photo.addClass('hide');
			$photo_tank.removeClass('hide');
			//disable sorting function@album mode
			$selected_photo.sortable("destroy");
		}else{
			window.location.hash = 'pick_photo';
			// initialize the photo array of sorted img
			$photo_tank.addClass('hide');
			$selected_photo.removeClass('hide');
			//refresh sorting function@pick_photo mode
			SortableDiv();
			GetSortArray();
			PhotoSizeAdjsut();
		}
		callback();
	});
}

//setting the margin of the img
function ImgRealign(){
	var $imgDiv = $(".imgDiv");
	$("img","#photo_tank #selected_photo").each(function(i,el){
		$(this).css({
			'margin-top':($(this).parent().height()-this.height)/2,
			'margin-left':($(this).parent().width()-this.width)/2,
		});
	});
}

// save the photo order of the slide
function SaveSlideOrder(){
	$.ajax({
		type:'POST',
		datatype:'JSON',
		data:{'id':window.slider_id,'slider_photos':window.slider_img},
		url:'/SaveSlideOrder/',
		success: function(Jdata){
			if(Jdata.error){
				console.log(Jdata.error)
			}else;
		},
		error: function(Jdata){
			console.log("picture order update failed!!");
		},
	});
}
// photo size setting and adjust position
function PhotoSizeAdjsut(callback){

	$(".imgDiv","#selected_photo").click(function(e){
		// console.log('imgDiv click.');
		e.preventDefault();
		var $modal_window = $('#modal_window');
		// reset div to empty
		$('img',$modal_window).remove();
		// set the div height
		var body_height = 650;
		$modal_window.height(body_height);
		// append img into contain
		var $img = $('img',$(this));
		var $contain = $('#modal_contain',$modal_window);
		$img.clone().height(body_height).appendTo($contain);
		// position img after display block
		var $target = $('img',$modal_window);

		function CheckModalDisplay(){
			setTimeout(function(){
				var modal_display = $('.modal').css('display').toString();
				if( modal_display == 'block' ){
					$target.css('left',($target.parent().width()-$target.width())/2).css('margin-left','').css('top','');
					// set contain size & center align the photo
					SetPhotoContainSize($target,body_height);
				}else{
					setTimeout(CheckModalDisplay(),300);
				}
			},300);
		}

		CheckModalDisplay();
		// photo draggable parameter setting
		$target.draggable({
			containment:"parent",
		});
		//zoom in & out setting
		$target.dblclick(function(){
			// adjust container
			// console.log('======'+(window.ratio+0.1)+'=========');
			PhotoContainerAdjust($(this));
			// adjust img 
			PhotoAlignAdjust($(this));
			// console.log('======'+(window.ratio+0.1)+'=========');
			window.ratio = RatioValue(window.ratio + 0.1);
		});
	});
}

// ======================================================================== //
// ======================= photo setting, zoom function =================== //
// ======================================================================== //

// set contain size
function SetPhotoContainSize(img,height){
	var $contain = img.parent();
	var win_width = $('.modal-dialog').width();
	// console.log('contian_w : '+$contain.width()/2+', img_w: '+img.width()+', win_width: '+win_width);
	$contain.height(height).width(win_width).css({
		'margin-top':'',
		'margin-left':'',
	});
	img.css({
		'left':win_width/2-img.width()/2,
		'top':'',
		'margin':'',
	});
}

// adjust img 
function PhotoContainerAdjust(img){
	// console.log(' ==================== container adjust. =========================== ');
	// console.log('ratio: '+window.ratio);
	var win_width = Math.round($('.modal-dialog').width());
	var $contain = img.parent();
	var ratio = RatioValue(window.ratio + 0.1);
	// adjust container height
	var contain_ratio = RatioValue(2*(ratio-1)+1);
	var contain_h = Math.round(contain_ratio*$contain.height()/(2*(window.ratio-1)+1));
	$contain.height(contain_h);
	$contain.css('margin-top', -( contain_h - $contain.parent().height() )/2 )
	// adjust container width
	var img_w = img.width()/window.ratio*ratio;
	var contain_w = 2*img_w - win_width;
	// console.log('img_w: '+img_w+',contain_width: '+contain_w+',contain is '+$contain.attr('id')+', window : '+win_width);
	if( img_w < win_width ){
		console.log('img < win_width.')
		$contain.width( win_width );
		$contain.css('margin-left',0);
		
	}else if( img_w > win_width ){
		console.log('img > win_width.')
		$contain.width( contain_w );
		$contain.css('margin-left', -( contain_w - win_width )/2 );
	}else;
	// console.log(' ===================== adjust end. ================== ');

}

function PhotoAlignAdjust(img){
	// console.log(' ================ img align and resize. ======================== ');
	// console.log('ratio: '+window.ratio);
	// resize photo
	var $contain = img.parent();
	var $modal_window = $('#modal_window');
	var ratio = RatioValue(window.ratio + 0.1);
	img.height( Math.round( $modal_window.height()*ratio ) ).width( 'auto' );
	// reposition photo
	var img_left = parseInt(img.css('left'));
	var img_top = parseInt(img.css('top'));
	var img_w = img.width();
	var img_h = img.height();
	var contain_w = parseInt( $contain.width() );
	var contain_h = parseInt( $contain.height() );
	// console.log('contain width: '+$contain.width()+ ', contain height: '+$contain.height());
	var max_left = Math.round( parseInt( contain_w - img_w ) );
	var max_top = Math.round( parseInt( contain_h - img_h ) );
	
	
	// console.log('img_left: '+img_left+', img_top: '+img_top);
	// console.log('max_left: '+max_left+', max_top: '+max_top);
	// console.log('img_w: '+img_w+', img_h: '+img_h);
	// console.log('contain_w: '+contain_w+', contain_h: '+contain_h);
	if( img_left >= max_left ){
		// console.log('need to change left.')
		if( (img_left+img_w) >= contain_w ){
			img.css('left', parseInt( contain_w - img_w ));
		}else{
			img.css('left',max_left);
		}
	}else;
		// console.log('no need to change left.');
	if( img_top >= max_top){
		// console.log('need to change top.');
		if( (img_top+img_h) >= contain_h ){
			img.css('top', parseInt( contain_h - img_h ));
		}else{
			img.css('top',max_top);
		}
	}else;
		// console.log('no need to change top.');


 	// console.log(' ================ img align and resize end. ======================== ');
 	// console.log('img height: '+$modal_window.height()*ratio );
	// console.log('img left: '+img.css('left'));
	// console.log('img top: '+img.css('top'));
}


function RatioValue(x){
	return Math.round(100*x)/100;
}
// ========================================================================= //
// ======================= rearrang photo order function =================== //
// ========================================================================= //


// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});
