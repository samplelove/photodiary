$(window).load(function(){
	// align function
	// AlignLine();
	AlignDiaryIcon();
	AlignHeaderIcon();
	ResizeDiaryDate();
	TimeSearch();
	// $('*').addClass('wireframe');
	
})
// fixlayout
$(window).resize(function(){
	AlignDiaryIcon();
	AlignHeaderIcon();
	ResizeDiaryDate();
});

function AlignHeaderIcon(){
	var win_width = $(window).width();
	var win_height = $(window).height();1
	// header height adjust
	var $header = $('#header');
	$header.height(win_height*0.17);
	// diary content height adjust
	var $diary_container = $('#diary_container');
	$diary_container.height(win_height*0.83-70);
	// header left side icon adjust
	var $diary_tank = $('#diary_tank');
	var $slider_tank = $('#slider_tank');
	var $header_line = $('.header_line');
	var $diary_parent = $diary_tank.parent();
	var $slider_parent = $slider_tank.parent();
	$diary_tank
		.width($diary_parent.width()/2+35)
		.height($diary_parent.width()/2+35)
		.css('margin-left',($diary_parent.width()-$diary_tank.width())/2);
	$slider_tank
		.width($slider_parent.width()/2+25)
		.height($slider_parent.width()/2+25);
	// center align arrow
	var $arrow = $('.arrow-down');
	$arrow.css('margin-left',$arrow.parent().width()*0.5 - 12);
	// align header line to fit on container top
	$header_line.height($header.height()-$diary_tank.height()-20).css('margin-top',$diary_tank.height()+3);

	// adjust the words of today,greeting..etc.
	var $header_info  = $("#header_info");
	var $today = $("#today");
	var t_ratio = $header_info.width()*0.20;
	$today.css({
		'font-size':t_ratio+'px',
		'line-height':t_ratio+'px',
		'margin-top':t_ratio*0.3+'px',
	});
	var $words = $("#weekday,#greeting,#festival");
	var word_ratio = $header_info.width()*0.13;
	$words.css({
		'font-size':word_ratio+'px',
		'line-height':word_ratio+'px',
	});
	// adjust the head_img hieght equal to its width and adjust its position
	var $head_div = $('#head_div');
	var $head_img = $('#head_img');
	$head_img.width($head_div.width()*0.8).css('margin-left',$head_div.width()*0.1);
	$head_div.css({
		'margin-top':-$head_img.height()*0.3,
	});

	// adjust search and add new diary function div
	var $snp = $('#snp');
	$snp.css({
		'margin-top':$head_div.height()*0.85,
	})
	// adjust search and add diary icon
	var $search_n_add = $("#search_n_add");
	var $search = $('#search');
	var $plus = $('#plus');
	var size = $search_n_add.height();
	$search.css({
		'width':size-4,
		'hieght':size-4,
		'margin-left':-size-10,
	});
	$plus.css({
		'width':size-4,
		'hieght':size-4,
		'margin-left':size+10,
	});

}
function ResizeDiaryDate(){
	var size = $('#diary_tank').width()*0.8;
	$('.diary_date').each(function(i,el){
		$(el).width(size).height(size)
			.css({
				'left':-size/2,
				'padding':size/10,
				'font-size':size/3,
		});
	});
	$('.diary_date_repeat').each(function(i,el){
		$(el).width(size/4).height(size/4)
			.css({
				'left':-size/4,
		});
	});
}
 


// photo drag view function setting
$('img','.photo_drag').load(function(){
	PhotoWidthCalculate();
});

// diary_func_div animation

$('.diary_func_div').parent().on('click',function(){
	// reset all css properties
	$('.diary_func_div').each(function(i,el){
		$(el).width(35).height(35);
		$(el).parent().css('background-color','');
	});
	$('.diary_date').each(function(i,el){
		$(el).css({'color':'','background-color':''});
	});
	$('.diary_title').each(function(i,el){
		$(el).css('color','rgb(255,255,255)');
	});
	// change the css properties of selected element
	$(this).css('background-color','rgba(255,255,255,0.7)')
	$('.diary_func_div',this).width(150).height(50);
	$('.diary_title',this).css('color','rgb(122,0,0)');
	AlignDiaryIcon();
	$('.diary_date',$(this).parent().parent()).css({'color':'rgb(122,0,0)','background-color':'rgba(255,255,255,0.8)'});


});

// add new diary 
$('#plus').click(function(){
	window.location.href = '/AddDiary/';
});

// diary_search function
$('#search').click(function(){
	$('#search_modal').modal();
});

// forword present page
$('.diary_title').click(function(){
	var page_id = $(this).attr('data-id');
	window.location.href = '/Diary/'+page_id;
});

// edit diary
$('.diary_edit').click(function(){
	console.log('hi hi.');
	var page_id = $(this).attr('data-id');
	window.location.href = '/EditDiary/'+page_id;
});

// facebook shared
$('.diary_share').click(function(){
	console.log('click.');
	var pageid = $(this).attr('data-id');
	$.ajax({
		type:'POST',
		datatype:'JSON',
		data:{'pageid':pageid},
		url:'/FBShare/',
		success:function(Jdata){
			 
			if(Jdata.error){
				alert(Jdata.error);
			}else{
				window.location.reload();
			}
		},
		error:function(Jdata){
			alert('Facebook share error!');
		}
	})
});

// diary delete 
$('.diary_delete').click(function(){
	console.log('click.');
	var pageid = $(this).attr('data-id');
	$.ajax({
		type:'POST',
		datatype:'JSON',
		data:{'pageid':pageid},
		url:'/DeleteDiary/',
		success:function(Jdata){
			 
			if(Jdata.error){
				alert(Jdata.error);
			}else{
				window.location.reload();
			}
		},
		error:function(Jdata){
			alert('Facebook share error!');
		}
	})
});

function TimeSearch(){
	var $search_year = $('#search_year');
	var $search_month = $('#search_month');
	$('#year_up').click(function(){
		var cy = parseInt($search_year.html());
		$search_year.html(cy+1);
	});
	$('#year_down').click(function(){
		var cy = parseInt($search_year.html());
		$search_year.html(cy-1);
	});
	$('#month_up').click(function(){
		var cm = parseInt($search_month.html());
		var str= '';
		if(cm < 10)
			str = '0'+(cm+1).toString();
		else
			str = (cm+1).toString();

		$search_month.html(str);
	});
	$('#month_down').click(function(){
		var cm = parseInt($search_month.html());
		var str= '';
		if(cm < 10)
			str = '0'+(cm-1).toString();
		else
			str = (cm-1).toString();
						
		$search_month.html(str);
	});
	// submit data to search diary 
	$('#diary_search').click(function(){		
		window.location.href = '/Home/'+$search_year.html()+'-'+$search_month.html();
	});
}
// align icon in the diary element
function AlignDiaryIcon(){
	var $diary_func_div = $(".diary_func_div");
	$diary_func_div.each(function(i,el){
		var left = $(el).parent().width() - $(el).width();
		$(el).css('margin-left',left);
	});
}
// calculate the width of the total photo width
function PhotoWidthCalculate(){
	// console.log('Calclate the Photo width. lenght: '+$('.photo_drag').length);
	$('.photo_drag').each(function(i,el){
		var img = $('img',this);
		var $target = $(el);		
		$(img).load(function(){
			// console.log('-------------------------');
			var total_width = 0;
			for(var i=0;i<img.length;i++){
				// console.log('width : '+total_width);
				// console.log('img '+$(img[i]).attr('src'));	
				total_width = total_width + $(img[i]).width() + 10;
			}
			// align the left of the darg container
			var drag_window_width = $target.parent().parent().width()
			var left = total_width - drag_window_width;
			$target.width(total_width);			
			// judge is neeed draggable
			if(total_width > drag_window_width ){
				$target.parent().width( total_width*2 - $target.parent().parent().width() ).css('margin-left',-left);
				$target.draggable({
					axis:'x',
					containment:'parent',
				}).css('left',left);
			}else;
			// console.log('-------------------------');
		});
		
	});
}
// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});