jQuery.fn.extend({
	//use example:   $('textarea').insertAtCaret( 'some string of characters' );
	vcenter: function(opt){
		var $parent = this.parent();
		this.css('margin-top',($parent.height()-this.height())/2);
	},
	hcenter: function(opt){
		var $parent = this.parent();
		this.css('margin-left',($parent.width()-this.width())/2);
	},
	center: function(opt){
		var $parent = this.parent();
		this.css('margin-top',($parent.height()-this.height())/2);
		this.css('margin-left',($parent.width()-this.width())/2);
	}
});