# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import datetime
import uuid
import os
from django.utils.http import urlquote
# Create your models here.
# def head_upload(Member,filename):
# 	file_name = uuid.uuid4().hex
# 	file_ext = os.path.splitext(filename)[-1]
# 	full_name = file_name + file_ext
# 	file_path = 'images/%s/%s/%s' % ('head_image',slider_id,full_name)
# 	return file_path


class Member(models.Model):
	user = models.OneToOneField(User)
	head_image = models.CharField(max_length=250,null=True)
	name = models.CharField(max_length=50)
	email = models.EmailField(max_length=100)
	sex = models.CharField(max_length=10)
	birthday = models.DateField(null=True)

class Diary(models.Model):
	member = models.ForeignKey(Member)
	diary_id = models.CharField(max_length=50)
	diary_content = models.CharField(max_length=5000)
	date = models.DateField()
	title = models.CharField(max_length=50,null=True,default="無")
	shared = models.BooleanField(default=False)

class Slider(models.Model):
	member = models.ForeignKey(Member)
	date = models.DateField()
	title = models.CharField(max_length=75)
	describe = models.CharField(max_length=100,default='') 
	slider_id = models.CharField(max_length=50)

class Slider_photo(models.Model):
	slider = models.ForeignKey(Slider)
	image_url = models.CharField(null=True,max_length=600)
	describe = models.CharField(max_length=75)
	order = models.IntegerField(max_length=15)
	positionLeft = models.FloatField(default=0)
	positionRight = models.FloatField(default=0)
	zoom = models.FloatField(default=1)

def photo_upload_to(photo_id,filename,folder):
	file_name = uuid.uuid4().hex
	file_ext = os.path.splitext(filename)[-1]
	full_name = file_name + file_ext 
	file_path = 'image/%s/%s/%s' % (folder,photo_id,full_name)
	return file_path

def photo_upload(Photo,filename):
	return photo_upload_to(Photo.diary.diary_id,filename,'ori_photo')

def resize_photo_upload(Photo,filename):
	return photo_upload_to(Photo.diary.diary_id,filename,'resize_photo')

class Photo(models.Model):
	diary = models.ForeignKey(Diary)
	origin_image = models.ImageField(upload_to=photo_upload)
	resize_image = models.ImageField(upload_to=resize_photo_upload)
	positionLeft = models.FloatField(default=0)
	positionRight = models.FloatField(default=0)
	zoom = models.FloatField(default=1)

def frame_upload(Frame,filename):
	return photo_upload_to(Frame.diary.dariy_id,filename,'frame') 

class Frame(models.Model):
	diary = models.ForeignKey(Diary)
	color = models.CharField(max_length=10)
	background_image = models.ImageField(upload_to=frame_upload)
	theme = models.IntegerField(null=True,default=0)
	background_color = models.CharField(max_length=10)

def theme_upload(Diarytheme,filename):
	file_name = uuid.uuid4().hex
	file_ext = os.path.splitext(filename)[-1]
	full_name = file_ext + filex_name
	theme = Diarytheme.theme
	file_path = 'images/%s/%s/%s' % ('theme',theme,full_name)
	return file_path

class DiaryTheme(models.Model):
	diary = models.ForeignKey(Diary)
	theme = models.IntegerField(default=0)
	background_image = models.ImageField(upload_to=theme_upload)
	icon  = models.ImageField(upload_to=theme_upload)
	logo = models.ImageField(upload_to=theme_upload)
	button =  models.ImageField(upload_to=theme_upload)

def news_image_upload(News,filename):
	file_name = uuid.uuid4().hex 
	file_format = os.path.splitext(filename)[-1]
	full_fine_name = file_name + file_format
	file_path = 'images/%s/%s/' % ('News',file_path)
	return file_path

class News(models.Model):
	date = models.DateTimeField(default=datetime.datetime.now(),null=True)
	content = models.CharField(max_length=2500,null=True)
	title = models.CharField(max_length=500)
	image_url = models.ImageField(upload_to=news_image_upload)


class News_photo(models.Model):
	news = models.ForeignKey(News)
	image_url = models.ImageField(upload_to=news_image_upload)